﻿using MazeGame.GameEvents;
using System.Collections.Generic;
using System.Linq;

namespace MazeGame.Turns
{
  public class TurnResult
  {
    private readonly List<GameEvent> _events = new List<GameEvent>();

    public IEnumerable<GameEvent> Events => _events;

    public bool MadeProgress { private get; set; }
    public bool NeedsRefresh => MadeProgress || _events.Any();

    public void AddEvent(GameEvent gameEvent) => _events.Add(gameEvent);

    public void ClearEvents() => _events.Clear();
  }
}
