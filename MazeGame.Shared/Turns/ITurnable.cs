﻿using MazeGame.Action;

namespace MazeGame.Turns
{
  public interface ITurnable
  {
    bool CanTakeTurn();
    bool IsWaitingForInput();
    bool GainEnergy();
    void FinishTurn();

    IAction GetAction();
  }
}
