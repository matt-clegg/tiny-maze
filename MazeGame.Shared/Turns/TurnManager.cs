﻿using MazeGame.Action;
using System.Collections.Generic;
using System.Linq;

namespace MazeGame.Turns
{
  public class TurnManager<T> where T : ITurnable
  {
    private readonly TurnResult _turnResult = new TurnResult();
    private int _currentIndex;

    public bool Running { get; set; } = true;
    public bool AdvanceTurnOnFailure { get; set; }

    private T CurrentTurnable(List<T> turnables) => _currentIndex < 0 || _currentIndex >= turnables.Count ? default(T) : turnables[_currentIndex];

    public TurnManager(bool advanceTurnOnFailure)
    {
      AdvanceTurnOnFailure = advanceTurnOnFailure;
    }

    public TurnResult Process(List<T> turnables)
    {
      _turnResult.MadeProgress = false;
      _turnResult.ClearEvents();

      if (!turnables.Any())
      {
        return _turnResult;
      }

      while (Running)
      {
        T turnable = CurrentTurnable(turnables);

        if (EqualityComparer<T>.Default.Equals(turnable, default(T)))
        {
          AdvanceIndex(turnables);
          continue;
        }

        if (turnable.CanTakeTurn() && turnable.IsWaitingForInput())
        {
          return _turnResult;
        }

        _turnResult.MadeProgress = true;

        IAction action = GetNextAction(turnables, ref turnable);

        if (action == null)
        {
          return _turnResult;
        }

        ActionResult result = PerformAction(action, turnable);

        if (result.Succeeded || AdvanceTurnOnFailure)
        {
          turnable.FinishTurn();
          AdvanceIndex(turnables);
        }
      }

      return _turnResult;
    }

    private IAction GetNextAction(List<T> turnables, ref T turnable)
    {
      IAction action = null;
      while (action == null)
      {
        turnable = CurrentTurnable(turnables);

        if (EqualityComparer<T>.Default.Equals(turnable, default(T)))
        {
          AdvanceIndex(turnables);
          continue;
        }

        if (turnable.CanTakeTurn() || turnable.GainEnergy())
        {
          if (turnable.IsWaitingForInput())
          {
            return null;
          }

          action = turnable.GetAction();
        }
        else
        {
          AdvanceIndex(turnables);
        }
      }

      return action;
    }

    private ActionResult PerformAction(IAction action, T turnable)
    {
      ActionResult result = action.Perform(turnable, _turnResult);
      while (result.Alternative != null)
      {
        action = result.Alternative;
        result = action.Perform(turnable, _turnResult);
      }

      return result;
    }

    private void AdvanceIndex(List<T> turnables) => _currentIndex = (_currentIndex + 1) % turnables.Count();
  }
}
