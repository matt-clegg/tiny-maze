﻿using MazeGame.Assets;
using MazeGame.Assets.DataLoading;
using MazeGame.Input;
using MazeGame.Scenes;
using MazeGame.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.IO;
using System.Reflection;
using System.Runtime;
using System.Windows.Forms;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;
using KeyEventArgs = MazeGame.Input.KeyEventArgs;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace MazeGame
{
    public class Engine : Game
    {
        private const string DebugTitleFormat = "{0} | {1} fps {2:F} MB";

        public static Engine Instance { get; private set; }

        private readonly string _title;

        public static int WindowWidth { get; private set; }
        public static int WindowHeight { get; private set; }
        public static int VirtualWidth { get; private set; }
        public static int VirtualHeight { get; private set; }

        private RenderTarget2D _renderTarget;
        private Rectangle _renderTargetDestination;

        private readonly GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private Scene _scene;
        private Scene _nextScene;

        public static Scene Scene {
            get => Instance._scene;
            set => Instance._nextScene = value;
        }

        public string AssemblyLocation { get; private set; }
        public string ContentLocation { get; private set; }

        private int _fpsCounter;
        private TimeSpan _timeElapsed;
        public static int FPS { get; private set; }

        private readonly AssetCache<string> _assets;
        public static AssetCache<string> Assets => Instance._assets;

        public IRandom Random { get; }

        private readonly DelayedInputHandler _input = new DelayedInputHandler(20);

        public Engine(string title, int windowWidth, int windowHeight, int virtualWidth, int virtualHeight)
        {
            Instance = this;

            AssemblyLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);


            if (Directory.Exists(AssemblyLocation + "\\Content"))
            {
                ContentLocation = AssemblyLocation + "\\Content";
            }
            else if (Directory.Exists(AssemblyLocation + "\\..\\Content"))
            {
                ContentLocation = AssemblyLocation + "\\..\\Content";
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Content folder could not be found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            Content.RootDirectory = ContentLocation;

            Random = new GameRandom();

            _title = title;
            WindowWidth = windowWidth;
            WindowHeight = windowHeight;
            VirtualWidth = virtualWidth;
            VirtualHeight = virtualHeight;

            _graphics = new GraphicsDeviceManager(this)
            {
                SynchronizeWithVerticalRetrace = true,
                PreferMultiSampling = false,
                GraphicsProfile = GraphicsProfile.HiDef,
                PreferredBackBufferFormat = SurfaceFormat.Color,
                PreferredDepthStencilFormat = DepthFormat.None,
                PreferredBackBufferWidth = WindowWidth,
                PreferredBackBufferHeight = WindowHeight
            };

            IsMouseVisible = true;

            _input.InputFireEvent += OnInputEvent;
            _assets = new AssetCache<string>();

            GCSettings.LatencyMode = GCLatencyMode.SustainedLowLatency;

            Window.Title = string.Format(DebugTitleFormat, title, 0, 0);
        }

        private void OnInputEvent(object sender, KeyEventArgs e)
        {
            Scene?.Input(e.Key);
        }

        protected override void Initialize()
        {
            _renderTarget = new RenderTarget2D(GraphicsDevice, VirtualWidth, VirtualHeight);
            _renderTargetDestination = new Rectangle(0, 0, WindowWidth, WindowHeight);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            var assetLoader = new AssetLoader(Assets, Content);

            try
            {
                assetLoader.Load($"{ContentLocation}\\Data\\spritesheets.json");
                assetLoader.Load($"{ContentLocation}\\Data\\sprites.json");
                assetLoader.Load($"{ContentLocation}\\Data\\tiles.json");
                assetLoader.Load($"{ContentLocation}\\Data\\items.json");
                assetLoader.Load($"{ContentLocation}\\Data\\creatures.json");
                assetLoader.LoadFont();

                Scene = new GameScene();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("There was an error when loading game data.\n\n" + ex.Message + "\n\n" + ex.StackTrace,
                                                      "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Exit();
            }
        }

        protected override void UnloadContent()
        {
            _spriteBatch.Dispose();
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }

            _input.Update(Keyboard.GetState());
            _scene?.Update(gameTime);

            if (_nextScene != null)
            {
                _scene = _nextScene;
                _nextScene = null;
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            DrawToRenderTarget();

            _spriteBatch.Begin(samplerState: SamplerState.PointClamp);
            _spriteBatch.Draw(_renderTarget, _renderTargetDestination, Color.White);
            _spriteBatch.End();

            // Show fps and memory usage in debug mode
            _fpsCounter++;
            _timeElapsed += gameTime.ElapsedGameTime;

            // Update once a second
            if (_timeElapsed >= TimeSpan.FromSeconds(1))
            {
                double memory = GC.GetTotalMemory(false) / 1048576f;
                Window.Title = string.Format(DebugTitleFormat, _title, _fpsCounter, memory);
                FPS = _fpsCounter;
                _fpsCounter = 0;
                _timeElapsed -= TimeSpan.FromSeconds(1);
            }

            base.Draw(gameTime);
        }

        private void DrawToRenderTarget()
        {
            GraphicsDevice.SetRenderTarget(_renderTarget);
            GraphicsDevice.Clear(Color.Gray);

            _spriteBatch.Begin(samplerState: SamplerState.PointClamp);
            _scene?.Draw(_spriteBatch);
            _spriteBatch.End();

            GraphicsDevice.SetRenderTarget(null);
        }
    }
}
