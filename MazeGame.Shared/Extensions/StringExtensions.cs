﻿using MazeGame.Util;
using System;

namespace MazeGame.Extensions
{
  public static class StringExtensions
  {

    public static int NumberFromRange(this string input, IRandom random)
    {
      string[] raw = input.Trim().Split('-', ',', ' ');

      if (raw.Length == 0)
      {
        return input.ParseInt();
      }

      if (raw.Length == 1)
      {
        return raw[0].ParseInt();
      }

      if (raw.Length == 2)
      {
        int min = raw[0].ParseInt();
        int max = raw[1].ParseInt();

        if (min > max)
        {
          throw new InvalidOperationException($"Min value must be less than max value: {min} < {max}");
        }
        return random.Next(min, max);
      }

      throw new InvalidOperationException($"Invalid range value: '{input}'");
    }

    private static int ParseInt(this string input)
    {
      if (int.TryParse(input.Trim(), out int result))
      {
        return result;
      }

      throw new InvalidOperationException($"Invalid integer: '{input}'");
    }
  }
}
