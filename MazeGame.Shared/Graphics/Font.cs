﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MazeGame.Graphics
{
  public class Font : GameTexture
  {
    private readonly string _charset;

    private readonly Rectangle[] _sources;

    public int CharWidth { get; }
    public int CharHeight { get; }

    public Font(string name, Texture2D texture, int charWidth, int charHeight, string charset) : base(name, texture)
    {
      _charset = charset.ToLower();
      CharWidth = charWidth;
      CharHeight = charHeight;

      int widthInChars = texture.Width / CharWidth;
      int heightInChars = texture.Height / CharHeight;

      _sources = new Rectangle[widthInChars * heightInChars];

      for (int i = 0; i < _sources.Length; i++)
      {
        int x = (i % widthInChars) * CharWidth;
        int y = (i / widthInChars) * CharHeight;

        _sources[i] = new Rectangle(x, y, CharWidth, CharHeight);
      }
    }

    public void Write(string text, int x, int y, Color color, SpriteBatch batch, bool shadow = true, int scale = 1)
    {
      WritePrecise(text, x * CharWidth * scale + (CharWidth * scale / 2), y * CharHeight * scale + (CharHeight * scale / 2), color, batch, shadow, scale);
    }

    public void WritePrecise(string text, int x, int y, Color color, SpriteBatch batch, bool shadow = true, int scale = 1)
    {
      if (string.IsNullOrWhiteSpace(text))
      {
        return;
      }

      text = text.ToLower();

      for (int i = 0; i < text.Length; i++)
      {
        int index = _charset.IndexOf(text[i]);

        if (index < 0 || index >= _sources.Length)
        {
          continue;
        }

        var destination = new Rectangle(x + i * (CharWidth * scale),
                                        y, CharWidth * scale, CharHeight * scale);

        Rectangle source = _sources[index];

        if (!shadow)
        {
          source.Y += CharHeight * 2;
        }

        batch.Draw(Texture, destination, source, color);
      }
    }
  }
}
