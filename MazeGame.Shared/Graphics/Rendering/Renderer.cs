﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MazeGame.Graphics.Rendering
{
  public abstract class Renderer
  {
    private static readonly Color DefaultSpriteColor = Color.White;

    public virtual void Update()
    {

    }

    public abstract void Draw(SpriteBatch batch);

    protected void DrawSprite(Sprite sprite, int x, int y, SpriteBatch batch, Color? color = null)
    {
      int xp = x * sprite.Width;
      int yp = y * sprite.Height;
      DrawSpritePrecise(sprite, xp, yp, batch, color);
    }

    protected void DrawSpritePrecise(Sprite sprite, int x, int y, SpriteBatch batch, Color? color = null)
    {
      var destination = new Rectangle(x, y, sprite.Width, sprite.Height);
      batch.Draw(sprite.Texture, destination, sprite.Bounds, color ?? DefaultSpriteColor);
    }
  }
}
