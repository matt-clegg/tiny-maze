﻿using MazeGame.Entities.Creatures;
using MazeGame.Entities.Items;
using MazeGame.Entities.Props;
using MazeGame.Util;
using MazeGame.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MazeGame.Graphics.Rendering
{
  public class RoomRenderer : Renderer
  {
    private const float FootprintsOpacity = 0.4f;

    private readonly Sprite _shadowSprite;
    private readonly Sprite _footprint;

    public Room Room { get; set; }

    public RoomRenderer(Room room = null)
    {
      Room = room;
      _shadowSprite = Engine.Assets.GetAsset<Sprite>("shadow");
      _footprint = Engine.Assets.GetAsset<Sprite>("footprint");
    }

    public override void Draw(SpriteBatch batch)
    {
      if (Room == null)
      {
        return;
      }

      DrawTiles(batch);
      DrawFootprints(batch);
      DrawShadows(batch);
      DrawProps(batch);
      DrawItems(batch);
      DrawCreatures(batch);
    }

    private void DrawTiles(SpriteBatch batch)
    {
      for (int x = 0; x < Room.Width; x++)
      {
        for (int y = 0; y < Room.Height; y++)
        {
          Tile tile = Room.GetTile(x, y);

          if (tile != null)
          {
            DrawSprite(tile.Sprite, x, y, batch);
          }

          string blood = Room.GetBlood(x, y);
          if (blood != null)
          {
            Sprite bloodSprite = Engine.Assets.GetAsset<Sprite>(blood);
            DrawSprite(bloodSprite, x, y, batch);
          }

        }
      }
    }

    private void DrawFootprints(SpriteBatch batch)
    {
      foreach (Point2D point in Room.Footprints.Keys)
      {
        float opacity = (Room.Footprints[point] / (float)Maze.FootprintAge) * FootprintsOpacity;
        DrawSpritePrecise(_footprint, point.X, point.Y, batch, Color.Black * opacity);
      }
    }

    private void DrawShadows(SpriteBatch batch)
    {
      for (int x = 0; x < Room.Width; x++)
      {
        for (int y = 0; y < Room.Height; y++)
        {
          if (Room.HasShadow(x, y))
          {
            DrawSprite(_shadowSprite, x, y, batch);
          }
        }
      }

    }

    private void DrawProps(SpriteBatch batch)
    {
      foreach (Prop prop in Room.Props)
      {
        prop.Draw(batch);
      }
    }

    private void DrawItems(SpriteBatch batch)
    {
      foreach (Item item in Room.Items)
      {
        item.Draw(batch);
      }
    }

    private void DrawCreatures(SpriteBatch batch)
    {
      foreach (Creature creature in Room.Creatures)
      {
        creature.Draw(batch);
      }
    }

  }
}
