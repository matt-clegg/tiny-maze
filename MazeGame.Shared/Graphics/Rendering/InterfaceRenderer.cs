﻿using MazeGame.Entities.Creatures;
using MazeGame.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MazeGame.Graphics.Rendering
{
  public class InterfaceRenderer : Renderer
  {
    private readonly Font _font;
    private readonly Player _player;

    private readonly Sprite _itemBackground;
    private readonly Sprite _wealthSprite;

    private int _tick;
    private bool _textFlash;

    public InterfaceRenderer(Font font, Player player)
    {
      _font = font;
      _player = player;

      _itemBackground = Engine.Assets.GetAsset<Sprite>("item_background");
      _wealthSprite = Engine.Assets.GetAsset<Sprite>("gem_3");
    }

    public override void Update()
    {
      if (_player.HasWon && _tick++ % 45 == 0)
      {
        _textFlash = !_textFlash;
      }
    }

    public override void Draw(SpriteBatch batch)
    {
      int itemCount = _player.ActionItems.Count;
      for (int i = 0; i < itemCount; i++)
      {
        Sprite itemSprite = _player.ActionItems[i].Sprite;

        int x = 24 + i * 18;
        int y = 12 * 16 - 4;

        if (i == _player.SelectedItemIndex)
        {
          y -= 6;
        }

        DrawSpritePrecise(_itemBackground, x, y, batch);
        DrawSpritePrecise(itemSprite, x, y, batch);
        _font.WritePrecise((i + 1).ToString(), x + 13, y + 13, Color.White, batch);
      }

      _font.WritePrecise("Level " + (_player.CurrentLevel + 1), 8, 6, Color.White, batch);
      DrawSpritePrecise(_wealthSprite, 6, 14, batch);
      _font.WritePrecise($"{_player.Wealth:N0}", 26, 19, Color.White, batch);

      if (_player.HasWon)
      {
        WriteCenter("You escaped the maze!", 30, Color.White, batch);
        WriteCenter($"It took you {Timer.ElapsedTime().Minutes}m {Timer.ElapsedTime().Seconds}s", 50, Color.DeepSkyBlue, batch);
        WriteCenter($"Total wealth {_player.Wealth:n0}", 60, Color.DeepSkyBlue, batch);

        if (_textFlash)
        {
          WriteCenter("Press R to restart", 80, Color.White, batch);
        }
      }
    }

    private void WriteCenter(string message, int y, Color color, SpriteBatch batch, int scale = 1)
    {
      int x = (Engine.VirtualWidth / 2) - (message.Length / 2) * _font.CharWidth * scale;
      _font.WritePrecise(message, x, y, color, batch, scale: scale);
    }
  }
}
