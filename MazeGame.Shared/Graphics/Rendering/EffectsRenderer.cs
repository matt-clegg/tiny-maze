﻿using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.Linq;
using Effect = MazeGame.GameEvents.Effect;

namespace MazeGame.Graphics.Rendering
{
  public class EffectsRenderer : Renderer
  {
    private readonly List<Effect> _effects;

    public EffectsRenderer(List<Effect> effects)
    {
      _effects = effects;
    }

    public override void Draw(SpriteBatch batch)
    {
      foreach (Effect effect in _effects.OrderBy( e => e.RenderOrder ))
      {
        effect.Draw(batch);
      }
    }
  }
}
