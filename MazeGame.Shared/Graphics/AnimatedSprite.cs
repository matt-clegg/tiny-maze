﻿using Microsoft.Xna.Framework;

namespace MazeGame.Graphics
{
  /// <summary>
  /// A collection of sprites that can be played in sequence and looped.
  /// </summary>
  public class AnimatedSprite
  {
    private readonly int _frameDuration;
    private readonly Sprite[] _frames;

    public bool IsLooped { get; set; }
    public bool IsRunning { get; set; }

    private int _currentFrame;
    private double _currentTime;

    public AnimatedSprite(int frameDuration, params Sprite[] frames) : this(frameDuration, 0, frames) { }

    public AnimatedSprite(int frameDuration, int startingFrame, params Sprite[] frames)
    {
      _frameDuration = frameDuration;
      _frames = frames;
      _currentFrame += startingFrame % _frames.Length;

      IsLooped = true;
      IsRunning = true;
    }

    public Sprite GetSprite => _frames[_currentFrame];

    public void Update(GameTime gameTime)
    {
      if (!IsRunning)
      {
        return;
      }

      _currentTime += gameTime.ElapsedGameTime.TotalMilliseconds;

      if (_currentTime >= _frameDuration)
      {
        if ( _currentFrame + 1 == _frames.Length - 1 && !IsLooped) {
          IsRunning = false;
        }

        _currentFrame = (_currentFrame + 1) % _frames.Length;
        _currentTime -= _frameDuration;
      }
    }

  }
}
