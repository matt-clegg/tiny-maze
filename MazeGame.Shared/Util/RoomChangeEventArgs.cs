﻿using MazeGame.World;
using System;

namespace MazeGame.Util
{
  public class RoomChangeEventArgs : EventArgs
  {
    public Room OldRoom { get; }
    public Room NewRoom { get; }

    public RoomChangeEventArgs(Room oldRoom, Room newRoom)
    {
      OldRoom = oldRoom;
      NewRoom = newRoom;
    }
  }
}
