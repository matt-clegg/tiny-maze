﻿using System;

namespace MazeGame.Util
{
  public class ConfigParserException : Exception
  {
    public ConfigParserException(string message) : base(message)
    {
    }
  }
}
