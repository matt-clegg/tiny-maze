﻿using System;
using System.Diagnostics;

namespace MazeGame.Util
{
  public static class Timer
  {
    private static readonly Stopwatch _watch = new Stopwatch();

    public static DateTime StartTime { get; private set; }
    public static DateTime EndTime { get; private set; }

    public static void Start()
    {
      StartTime = DateTime.UtcNow;
      _watch.Start();
    }

    public static void End()
    {
      EndTime = DateTime.UtcNow;
      _watch.Stop();
    }

    public static void Reset() => _watch.Reset();

    public static TimeSpan ElapsedTime() => _watch.Elapsed;

  }
}
