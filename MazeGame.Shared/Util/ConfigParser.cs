﻿using System.IO;

namespace MazeGame.Util
{
  public static class ConfigParser
  {

    public static Config Load(string fileName)
    {
      if (!File.Exists(fileName))
      {
        throw new FileNotFoundException($"{fileName} not found.");
      }

      string[] lines = File.ReadAllLines(fileName);

      return LoadConfig(lines);
    }

    private static Config LoadConfig(string[] lines)
    {
      var config = new Config();

      for (int i = 0; i < lines.Length; i++)
      {
        string line = lines[i];

        if (string.IsNullOrWhiteSpace(line))
        {
          continue;
        }

        string[] split = line.Trim().Split(':');
        if (split.Length != 2)
        {
          throw new ConfigParserException($"Invalid line ({i + 1}): {line}");
        }

        string name = split[0].Trim().ToLowerInvariant();
        string value = split[1].Trim().ToLowerInvariant();

        config.Set(name, value);
      }

      return config;
    }
  }
}
