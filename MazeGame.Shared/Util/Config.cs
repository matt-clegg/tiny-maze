﻿using System.Collections.Generic;

namespace MazeGame.Util
{
  public class Config
  {
    private readonly Dictionary<string, object> _properties = new Dictionary<string, object>();

    public void Set(string name, object value)
    {
      if (_properties.ContainsKey(name))
      {
        _properties[name] = value;
      }
      _properties.Add(name, value);
    }

    private object GetRaw(string name)
    {
      name = name.Trim();
      if (_properties.TryGetValue(name, out object value))
      {
        return value;
      }

      throw new KeyNotFoundException($"Property does not exists: '{name}'");
    }

    public bool Has(string name) => _properties.ContainsKey(name);

    public object Get(string name) => GetRaw(name);

    public string GetString(string name) => Get(name).ToString();
    public byte GetByte(string name) => byte.Parse(GetString(name));
    public char GetChar(string name) => char.Parse(GetString(name));
    public short GetShort(string name) => short.Parse(GetString(name));
    public int GetInt(string name) => int.Parse(GetString(name));
    public float GetFloat(string name) => float.Parse(GetString(name));
    public double GetDouble(string name) => double.Parse(GetString(name));
    public long GetLong(string name) => long.Parse(GetString(name));
    public bool GetBool(string name) => bool.Parse(GetString(name));
  }
}
