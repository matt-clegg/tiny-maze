﻿using System.Collections.Generic;

namespace MazeGame.Util
{
  public class WeightedList<TItem>
  {
    private readonly IRandom _random;
    private int _totalWeight;

    private readonly Dictionary<TItem, int> _items = new Dictionary<TItem, int>();

    public WeightedList(IRandom random = null)
    {
      _random = random ?? new GameRandom();
    }

    public void AddItem(TItem item, int weight)
    {
      _items.Add(item, weight);
      _totalWeight += weight;
    }

    public TItem Get()
    {
      TItem selected = default(TItem);
      int index = _random.Next(_totalWeight);

      foreach (var kvp in _items)
      {
        int weight = kvp.Value;

        if (index < weight)
        {
          selected = kvp.Key;
          break;
        }

        index -= weight;
      }

      return selected;
    }
  }
}
