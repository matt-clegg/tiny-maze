﻿using MazeGame.Entities.Creatures;
using MazeGame.Entities.Items;
using MazeGame.Entities.Props;
using MazeGame.GameEvents;
using MazeGame.Turns;
using MazeGame.Util;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace MazeGame.World
{
  public class Room
  {
    public Point2D Position { get; }
    public int Level { get; }
    public bool IsStart { get; set; }
    public bool IsEnd { get; set; }

    private readonly string _wallType;
    private readonly string _floorType;

    private readonly Dictionary<Point2D, Room> _passages = new Dictionary<Point2D, Room>();
    private readonly Array2D<Tile> _tiles;
    private readonly Array2D<bool> _shadows;
    private readonly Array2D<string> _blood;

    private readonly Dictionary<Point2D, Prop> _props;
    private readonly Dictionary<Point2D, Item> _items;
    private readonly List<Creature> _creatures;
    private readonly List<Creature> _creaturesToRemove = new List<Creature>();

    public Dictionary<Point2D, int> Footprints { get; } = new Dictionary<Point2D, int>();

    public List<Creature> Creatures => _creatures;
    public IEnumerable<Prop> Props => _props.Values;
    public IEnumerable<Item> Items => _items.Values;

    private readonly TurnManager<Creature> _turnManager = new TurnManager<Creature>(true);

    private readonly bool _hasDrips;

    public Room(Array2D<Tile> tiles,
                Array2D<bool> shadows,
                Dictionary<Point2D, Item> items,
                List<Creature> creatures,
                Dictionary<Point2D, Prop> props,
                Point2D position,
                int level,
                string wallType,
                string floorType,
                bool hasDrips)
    {
      _tiles = tiles;
      _shadows = shadows;
      _items = items;
      _creatures = creatures;
      _props = props;
      _blood = new Array2D<string>(Width, Height);
      Position = position;
      Level = level;
      _wallType = wallType;
      _floorType = floorType;
      _hasDrips = hasDrips;
    }

    public bool IsEmpty(bool countPlayer = false)
    {
      return _items.Count == 0 && _creatures.Count == (countPlayer ? 0 : 1);
    }

    public int Width => _tiles.Width;
    public int Height => _tiles.Height;

    public void Init(Maze maze)
    {
      foreach (Creature creature in _creatures)
      {
        creature.Init(maze, Position);
      }
    }

    public TurnResult Update(GameTime gameTime)
    {
      TurnResult result = _turnManager.Process(Creatures);

      if (_hasDrips && Engine.Instance.Random.NextDouble() < 0.02)
      {
        int x = Engine.Instance.Random.Next(1, Width - 1);
        int y = Engine.Instance.Random.Next(1, Height - 1);

        Tile tile = GetTile(x, y);

        if (!tile.IsSolid && !(GetProp( x, y )?.IsSolid ?? false))
        {
          result.AddEvent(new GameEvent(new DripEffect(x, y)));
        }
      }

      foreach (Creature creature in Creatures)
      {
        if (creature.ShouldRemove)
        {
          RemoveCreature(creature);
        }
        else
        {
          creature.Update(gameTime);
        }
      }

      foreach (Item item in Items)
      {
        item.Update(gameTime);
      }

      return result;
    }

    public void UpdateFootprints()
    {
      var keys = new List<Point2D>(Footprints.Keys);
      foreach (Point2D point in keys)
      {
        Footprints[point] = Footprints[point] - 1;
        if (Footprints[point] <= 0)
        {
          Footprints.Remove(point);
        }
      }
    }

    public void AddFootprintAtTile(int x, int y)
    {
      int xp = x * 16 + Engine.Instance.Random.Next(-2, 2);
      int yp = y * 16 + Engine.Instance.Random.Next(-2, 2);

      var point = new Point2D(xp, yp);

      if (!Footprints.ContainsKey(point))
      {
        Footprints.Add(point, Maze.FootprintAge);
      }
      else
      {
        Footprints[point] = Maze.FootprintAge;
      }
    }

    public void AddBlood(int x, int y)
    {
      if (_blood.InBounds(x, y))
      {
        _blood[x, y] = "blood_" + Engine.Instance.Random.Next(1, 4);
      }
    }

    public string GetBlood(int x, int y) => _blood.InBounds(x, y) ? _blood[x, y] : null;

    public bool HasShadow(int x, int y) => _shadows.InBounds(x, y) && _shadows[x, y];

    public Tile GetTile(int x, int y) => _tiles.InBounds(x, y) ? _tiles[x, y] : null;

    public void SetTile(int x, int y, Tile tile)
    {
      if (_tiles.InBounds(x, y))
      {
        _tiles[x, y] = tile;
      }
    }

    public Prop GetProp(int x, int y) => _props.TryGetValue(new Point2D(x, y), out Prop prop) ? prop : null;

    public void AddProp(int x, int y, Prop prop)
    {
      if (_tiles.InBounds(x, y))
      {
        var point = new Point2D(x, y);

        if (_props.ContainsKey(point))
        {
          _props.Remove(point);
        }

        _props.Add(new Point2D(x, y), prop);
        prop.X = x;
        prop.Y = y;
      }
    }

    public Item GetItem(int x, int y) => _items.TryGetValue(new Point2D(x, y), out Item item) ? item : null;

    public bool RemoveItem(Item item) => _items.Remove(new Point2D(item.X, item.Y));

    public Creature GetCreature(int x, int y)
    {
      foreach (Creature creature in Creatures)
      {
        if (creature.X == x && creature.Y == y)
        {
          return creature;
        }
      }

      return null;
    }

    public void AddCreature(Creature creature, Point2D point) => AddCreature(creature, point.X, point.Y);

    public void AddCreature(Creature creature, int x, int y)
    {
      creature.X = x;
      creature.Y = y;
      creature.CurrentRoomPosition = Position;
      Creatures.Add(creature);
    }

    /// <summary>
    /// Removes a specific creature from the room.
    /// </summary>
    /// <param name="creature"></param>
    public void RemoveCreature(Creature creature)
    {
      _creaturesToRemove.Add(creature);
    }

    /// <summary>
    /// Removes all creatures that have been flagged for removal.
    /// </summary>
    public void RemoveCreatures()
    {
      foreach (Creature creature in _creaturesToRemove)
      {
        Creatures.Remove(creature);
      }

      _creaturesToRemove.Clear();

      if (IsEmpty())
      {
        OpenPassages();
      }
    }

    /// <summary>
    /// Gets the adjacent room a passageway is connected to.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public Room GetPassageConnectionAt(int x, int y) => _passages.TryGetValue(new Point2D(x, y), out Room room) ? room : null;

    /// <summary>
    /// Whether or not there is a passageway in a given direction.
    /// </summary>
    /// <param name="direction"></param>
    /// <returns></returns>
    public bool HasPassageInDirection(Point2D direction)
    {
      return _passages.ContainsKey(DirectionToEdge(direction));
    }

    /// <summary>
    /// Opens all passageways in this room.
    /// </summary>
    public void OpenPassages()
    {
      foreach (Point2D passage in _passages.Keys)
      {
        OpenDoorAt(passage.X, passage.Y);

        // Open the passage on the other side
        Room connectedRoom = _passages[passage];
        connectedRoom.OpenPassageTo(this);
      }
    }

    /// <summary>
    /// Opens a passageway to the given room.
    /// </summary>
    /// <param name="room"></param>
    public void OpenPassageTo(Room room)
    {
      Point2D passageDirection = room.Position - Position;
      Point2D passagePoint = DirectionToEdge(passageDirection);
      OpenDoorAt(passagePoint.X, passagePoint.Y);
    }

    private void OpenDoorAt(int x, int y)
    {
      if (GetProp(x, y) is Door door)
      {
        door.IsOpen = true;
      }
    }

    /// <summary>
    /// Adds a passageway to an adjacent room.
    /// </summary>
    /// <param name="random"></param>
    /// <param name="connectedRoom"></param>
    public void AddPassage(IRandom random, Room connectedRoom)
    {
      Point2D passageDirection = connectedRoom.Position - Position;

      Point2D passagePosition = DirectionToEdge(passageDirection);
      _passages.Add(passagePosition, connectedRoom);

      // Add shadows if connection is horizontal
      if (passageDirection.X != 0)
      {
        SetTile(passagePosition.X, passagePosition.Y - 1, Tile.RandomWall(random, _wallType, "h", 4));
        _shadows[passagePosition.X, passagePosition.Y] = true;
      }

      // Remove shadows if connection is vertical
      if (passagePosition.Y == 0)
      {
        _shadows[passagePosition.X, passagePosition.Y + 1] = false;
      }

      var door = new Door("door", passageDirection.X != 0)
      {
        X = passagePosition.X,
        Y = passagePosition.Y
      };

      SetTile(passagePosition.X, passagePosition.Y, Tile.RandomFloor(random, _floorType, 5));
      AddProp(passagePosition.X, passagePosition.Y, door);
    }

    /// <summary>
    /// Converts a direction (0, 1) or (-1,0) to a position on the edge of the room.
    /// </summary>
    /// <param name="direction"></param>
    /// <returns></returns>
    private Point2D DirectionToEdge(Point2D direction)
    {
      switch (direction.X)
      {
        case -1: // Left
          direction = new Point2D(0, Height / 2);
          break;
        case 1: // Right
          direction = new Point2D(Width - 1, Height / 2);
          break;
        case 0:
          direction = new Point2D(0, direction.Y);
          break;
      }

      switch (direction.Y)
      {
        case -1: // Up
          direction = new Point2D(Width / 2, 0);
          break;
        case 1: // Down
          direction = new Point2D(Width / 2, Height - 1);
          break;
        case 0:
          direction = new Point2D(direction.X, 0);
          break;
      }

      return direction;
    }

    /// <summary>
    /// Finds the position where the player enters this room when entering from the given room.
    /// </summary>
    /// <param name="otherRoom"></param>
    /// <returns></returns>
    public Point2D GetSpawnFrom(Room otherRoom)
    {
      if (otherRoom.Level != Level)
      {
        return new Point2D(Width / 2, Height / 2);
      }

      Point2D direction = otherRoom.Position - Position;
      Point2D doorTile = DirectionToEdge(direction);

      var inverse = new Point2D(direction.X * -1, direction.Y * -1);

      return doorTile + inverse;
    }

  }
}
