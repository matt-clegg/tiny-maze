﻿using MazeGame.Util;

namespace MazeGame.World
{
  public class Maze
  {
    public const int FootprintAge = 300;

    private readonly Level[] _levels;

    public Maze(Level[] levels)
    {
      _levels = levels;
    }

    public int TotalLevels => _levels.Length;

    public Level GetLevel(int level) => level >= 0 && level <= _levels.Length - 1 ? _levels[level] : null;
    public Room GetRoom(int x, int y, int level) => GetRoom(new Point2D(x, y), level);
    public Room GetRoom(Point2D point, int level) => GetLevel(level).GetRoom(point);

    public void Init()
    {
      foreach (Level level in _levels)
      {
        level.Init(this);
      }
    }

    public void Update(int level)
    {
      GetLevel(level)?.Update();
    }

    public void UpdateFootprints(int level)
    {
      foreach (Room room in GetLevel(level).Rooms)
      {
        room.UpdateFootprints();
      }
    }
  }
}
