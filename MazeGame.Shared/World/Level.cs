﻿using MazeGame.Util;
using System.Collections.Generic;

namespace MazeGame.World
{
  public class Level
  {
    private readonly Dictionary<Point2D, Room> _rooms;

    public IEnumerable<Room> Rooms => _rooms.Values;

    public int WidthInRooms { get; }
    public int HeightInRooms { get; }

    public int LevelNumber { get; }
    public Room StartingRoom { get; }
    public Room EndingRoom { get; }

    public Level(int levelNumber, int widthInRooms, int heightInRooms, Room startingRoom, Room endingRoom, Dictionary<Point2D, Room> rooms)
    {
      LevelNumber = levelNumber;
      WidthInRooms = widthInRooms;
      HeightInRooms = heightInRooms;
      StartingRoom = startingRoom;
      EndingRoom = endingRoom;
      _rooms = rooms;
    }

    public Room GetRoom(int x, int y) => GetRoom(new Point2D(x, y));
    public Room GetRoom(Point2D point) => _rooms.TryGetValue(point, out Room room) ? room : null;

    public void Init(Maze maze)
    {
      foreach (Room room in _rooms.Values)
      {
        room.Init(maze);
      }
    }

    public void Update()
    {
      foreach (Room room in _rooms.Values)
      {
        room.RemoveCreatures();
      }
    }
  }
}
