﻿using MazeGame.Graphics;
using MazeGame.Util;
using System;

namespace MazeGame.World
{
  public class Tile : IEquatable<Tile>
  {
    public string Name { get; }
    public Sprite Sprite { get; }
    public bool IsSolid { get; }

    public Tile(string name, Sprite sprite, bool isSolid)
    {
      Name = name;
      Sprite = sprite;
      IsSolid = isSolid;
    }

    public static Tile RandomWall(IRandom random, string type, string direction, int count)
    {
      Tile[] walls = new Tile[count];

      for (int i = 0; i < count; i++)
      {
        walls[i] = Engine.Assets.GetAsset<Tile>($"{type}_wall_{i + 1}_{direction}");
      }

      return walls[random.Next(walls.Length)];
    }

    public static Tile RandomCarpet(IRandom random)
    {
      Tile[] carpets =
      {
        Engine.Assets.GetAsset<Tile>( "carpet_floor_1" ),
        Engine.Assets.GetAsset<Tile>( "carpet_floor_2" ),
        Engine.Assets.GetAsset<Tile>( "carpet_floor_3" ),
      };

      return carpets[random.Next(carpets.Length)];
    }

    public static Tile RandomCarpetFeature(IRandom random)
    {
      return random.NextDouble() < 0.5
               ? Engine.Assets.GetAsset<Tile>("carpet_floor_feature_1")
               : Engine.Assets.GetAsset<Tile>("carpet_floor_feature_2");
    }

    public static Tile RandomWallFeature(IRandom random, string type, string direction)
    {
      Tile[] walls =
      {
        Engine.Assets.GetAsset<Tile>( $"{type}_wall_feature_{direction}" ),
        Engine.Assets.GetAsset<Tile>( $"{type}_wall_cracked_{direction}" ),
      };

      return walls[random.Next(walls.Length)];
    }

    public static Tile RandomFloor(IRandom random, string type, int count)
    {
      Tile[] floors = new Tile[count];

      for (int i = 0; i < count; i++)
      {
        floors[i] = Engine.Assets.GetAsset<Tile>($"{type}_floor_{i + 1}");
      }

      return floors[random.Next(floors.Length)];
    }

    public bool Equals(Tile other)
    {
      if (ReferenceEquals(null, other)) return false;
      if (ReferenceEquals(this, other)) return true;
      return string.Equals(Name, other.Name);
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      if (obj.GetType() != this.GetType()) return false;
      return Equals((Tile)obj);
    }

    public override int GetHashCode()
    {
      return (Name != null ? Name.GetHashCode() : 0);
    }
  }
}
