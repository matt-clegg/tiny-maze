﻿using MazeGame.Entities.Props;
using MazeGame.Graphics;
using MazeGame.Util;
using System.Collections.Generic;
using System.Linq;

namespace MazeGame.World.Generator
{
  public class LevelGenerator : IGenerator<Level>
  {
    private readonly IRandom _random;

    private readonly Config _worldConfig;
    private readonly Dictionary<Point2D, Room> _rooms = new Dictionary<Point2D, Room>();
    private readonly int _level;

    private readonly string _wallType;
    private readonly string _floorType;

    private Room _startRoom;
    private Room _endRoom;

    public LevelGenerator(Config worldConfig, int level, string wallType, string floorType, IRandom random = null)
    {
      _worldConfig = worldConfig;
      _level = level;
      _wallType = wallType;
      _floorType = floorType;
      _random = random ?? new GameRandom();
    }

    private int WidthInRooms => _worldConfig.GetInt("width_in_rooms");
    private int HeightInRooms => _worldConfig.GetInt("height_in_rooms");

    private int RoomCountMin => _worldConfig.GetInt("room_count_min");
    private int RoomCountMax => _worldConfig.GetInt("room_count_max");

    public Level Build()
    {
      return new Level(_level, WidthInRooms, HeightInRooms, _startRoom, _endRoom, _rooms);
    }

    public IGenerator<Level> Generate()
    {
      // Pick a random corner to place the starting room
      var startPosition = new Point2D(_random.Next(2) * (WidthInRooms - 1), _random.Next(2) * (HeightInRooms - 1));

      _startRoom = new RoomGenerator(startPosition, _level, _random, _worldConfig, _wallType, _floorType, isStart: true)
                   .Generate()
                   .Build();

      _rooms.Add(startPosition, _startRoom);

      // Pick a random amount of rooms to build
      int roomsToBuild = _random.Next(RoomCountMin, RoomCountMax);

      while (_rooms.Count < roomsToBuild)
      {
        if (_rooms.Count >= WidthInRooms * HeightInRooms)
        {
          break;
        }

        // Pick a random room and a random cardinal direction
        Room randomRoom = _rooms.Values.OrderBy(x => _random.Next()).First();
        Point2D randomDirection = Direction.CardinalDirections.OrderBy(x => _random.Next()).First();

        // Check if the room already has a passage in the random direction
        if (randomRoom.HasPassageInDirection(randomDirection))
        {
          continue;
        }

        // The position of the room to connect to
        Point2D roomToConnectPosition = randomRoom.Position + randomDirection;

        // Make sure the connection is within the maze bounds.
        if (roomToConnectPosition.X < 0 || roomToConnectPosition.Y < 0 || roomToConnectPosition.X >= WidthInRooms ||
             roomToConnectPosition.Y >= HeightInRooms)
        {
          continue;
        }

        // If the direction we are connecting to already has a room, use that, else generate a new room
        if (!_rooms.TryGetValue(roomToConnectPosition, out Room roomToConnect))
        {
          roomToConnect = new RoomGenerator(roomToConnectPosition, _level, _random, _worldConfig, _wallType, _floorType)
                          .Generate()
                          .Build();
        }

        // Connect the two rooms together and add the new room
        ConnectRooms(randomRoom, roomToConnect);

        // Add the new room if it doesn't already exist
        if (!_rooms.ContainsKey(roomToConnectPosition))
        {
          _rooms.Add(roomToConnectPosition, roomToConnect);
        }
      }

      // Pick a random room (that isn't the start room) as the end room
      do
      {
        _endRoom = _rooms.Values.OrderBy(x => _random.Next()).First();
      } while (_endRoom.Position.Equals(_startRoom.Position));

      _endRoom.IsEnd = true;

      AddStairs(_startRoom);
      AddStairs(_endRoom);

      return this;
    }

    private void AddStairs(Room room)
    {
      string name;

      if (room.IsStart)
      {
        name = "stairs_up";
      }
      else if (room.IsEnd)
      {
        name = "stairs_down";
      }
      else
      {
        return;
      }

      var prop = new Prop(name, false, Engine.Assets.GetAsset<Sprite>(name));
      room.AddProp(room.Width / 2, room.Height / 2, prop);
    }

    private void ConnectRooms(Room roomA, Room roomB)
    {
      roomA.AddPassage(_random, roomB);
      roomB.AddPassage(_random, roomA);
    }

  }
}
