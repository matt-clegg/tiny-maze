﻿using MazeGame.Ai;
using MazeGame.Entities.Creatures;
using MazeGame.Entities.Items;
using MazeGame.Entities.Props;
using MazeGame.Graphics;
using MazeGame.Util;
using System;
using System.Collections.Generic;

namespace MazeGame.World.Generator
{
  public class RoomGenerator : IGenerator<Room>
  {
    private readonly IRandom _random;
    private readonly Config _worldConfig;

    private readonly string _wallType;
    private readonly string _floorType;

    private readonly Point2D _position;
    private readonly int _level;

    private readonly bool _isStart;

    private readonly Array2D<Tile> _tiles;
    private readonly Array2D<bool> _shadows;
    private readonly Dictionary<Point2D, Item> _items = new Dictionary<Point2D, Item>();
    private readonly List<Creature> _creatures = new List<Creature>();
    private readonly Dictionary<Point2D, Prop> _props = new Dictionary<Point2D, Prop>();

    public RoomGenerator(Point2D position, int level, IRandom random, Config worldConfig, string wallType, string floorType, bool isStart = false)
    {
      _position = position;
      _level = level;
      _random = random;
      _worldConfig = worldConfig;
      _wallType = wallType;
      _floorType = floorType;

      _isStart = isStart;

      _tiles = new Array2D<Tile>(RoomWidth, RoomHeight);
      _shadows = new Array2D<bool>(RoomWidth, RoomHeight);
    }

    private int RoomWidth => _worldConfig.GetInt("room_width");
    private int RoomHeight => _worldConfig.GetInt("room_height");

    private double TreasureSpawnChance => _worldConfig.GetDouble("treasure_spawn_chance");
    private double ThreatSpawnChance => _worldConfig.GetDouble("threat_spawn_chance");

    private double WeightedThreatSpawnChance => ThreatSpawnChance * (1.0 + (_level * 0.125));

    public Room Build()
    {
      bool hasDrips = _floorType.Equals("cobble") || _wallType.Equals( "mossy" ) && _random.NextDouble() < 0.25;

      return new Room(_tiles, _shadows, _items, _creatures, _props, _position, _level, _wallType, _floorType, hasDrips)
      {
        IsStart = _isStart
      };
    }

    public IGenerator<Room> Generate()
    {
      for (int x = 0; x < RoomWidth; x++)
      {
        for (int y = 0; y < RoomHeight; y++)
        {

          // Floors
          SetTile(x, y, Tile.RandomFloor(_random, _floorType, 5));

          // Top walls
          if (y == 0)
          {
            SetTile(x, y, GetRandomWall("h"));
          }

          // Left and right walls
          if (x == 0 || x == RoomWidth - 1)
          {
            SetTile(x, y, GetRandomWall("v"));
          }

          // Bottom walls
          if (y == RoomHeight - 1)
          {
            SetTile(x, y, GetRandomWall("h"));
          }
        }
      }

      if (_random.NextDouble() < 0.65)
      {
        AddPillars();
      }

      Decorate();
      AddStartItem();
      AddShadows();

      return this;
    }

    private void AddShadows()
    {
      for (int x = 1; x < RoomWidth - 1; x++)
      {
        for (int y = 1; y < RoomHeight - 1; y++)
        {
          if (GetTile(x, y - 1).IsSolid && !GetTile(x, y).IsSolid)
          {
            _shadows[x, y] = true;
          }
        }
      }
    }

    private Tile GetRandomWall(string direction)
    {
      return _random.NextDouble() < 0.1 ?
               Tile.RandomWallFeature(_random, _wallType, direction) :
               Tile.RandomWall(_random, _wallType, direction, 4);
    }

    private void AddPillars()
    {
      int widthMod = _random.NextDouble() < 0.5 ? 2 : 4;
      int heightMod = _random.NextDouble() < 0.5 ? 2 : 4;

      if (widthMod == 2 && heightMod == 2)
      {
        if (_random.NextDouble() < 0.5)
        {
          widthMod = 4;
        }
        else
        {
          heightMod = 4;
        }
      }

      for (int x = 1; x < RoomWidth - 1; x++)
      {
        for (int y = 1; y < RoomHeight - 1; y++)
        {
          if (x % widthMod == 0 && y % heightMod == 0 && _random.NextDouble() < 0.75)
          {
            SetTile(x, y, GetRandomWall("h"));
          }
        }
      }
    }

    private void AddEdgeProp(string[] names, double chance)
    {
      string[] candles =
      {
        "stalagmite_1",
        "stalagmite_2"
      };

      for (int x = 1; x < RoomWidth - 1; x++)
      {
        for (int y = 1; y < RoomHeight - 1; y++)
        {
          if (x == 1 || y == 1 || x == RoomWidth - 2 || y == RoomHeight - 2)
          {

            if (x == RoomWidth / 2 || y == RoomHeight / 2)
            {
              continue;
            }

            if (_random.NextDouble() < chance)
            {
              string name = names[_random.Next(names.Length)];
              AddProp(x, y, new Prop(name, true, Engine.Assets.GetAsset<Sprite>(name)));
            }
          }
        }
      }
    }

    private void AddCarpet(int x, int y, int width, int height, bool centerFeature)
    {
      for (int xp = 0; xp < width; xp++)
      {
        int xa = xp + x;
        for (int yp = 0; yp < height; yp++)
        {
          int ya = yp + y;

          if (GetTile(xa, ya).IsSolid || GetProp(xa, ya) != null)
          {
            continue;
          }

          if (centerFeature && xp == width / 2 && yp == height / 2)
          {
            SetTile(xa, ya, Tile.RandomCarpetFeature(_random));
          }
          else if (!centerFeature && xp % 2 == (width % 2 != 0 ? 1 : 0) && yp % 2 == (height % 2 != 0 ? 1 : 0))
          {
            SetTile(xa, ya, Tile.RandomCarpetFeature(_random));
          }
          else
          {
            SetTile(xa, ya, Tile.RandomCarpet(_random));
          }
        }
      }
    }

    private void AddWebs()
    {
      for (int x = 1; x < RoomWidth - 1; x++)
      {
        for (int y = 1; y < RoomHeight - 1; y++)
        {
          if (_random.NextDouble() < 0.4 || GetProp(x, y) != null)
          {
            continue;
          }

          bool north = GetTile(x, y - 1).IsSolid;
          bool south = GetTile(x, y + 1).IsSolid;
          bool east = GetTile(x + 1, y).IsSolid;
          bool west = GetTile(x - 1, y).IsSolid;

          string webName = null;

          if (north && west)
          {
            webName = "web_tr";
          }
          else if (north && east)
          {
            webName = "web_tl";
          }
          else if (south && west)
          {
            webName = "web_br";
          }
          else if (south && east)
          {
            webName = "web_bl";
          }
          else if ((north || south || east || west) && _random.NextDouble() < 0.1)
          {
            webName = "web";
          }

          if (webName != null)
          {
            AddProp(x, y, new Prop(webName, false, Engine.Assets.GetAsset<Sprite>(webName)));
          }
        }
      }
    }

    private void AddAlters()
    {
      if (!_isStart)
      {
        AddProp(RoomWidth / 2, RoomHeight / 2, new Prop("alter", true, Engine.Assets.GetAsset<Sprite>("alter_" + (_random.NextDouble() < 0.5 ? 1 : 2))));
        AddBones();
      }
    }

    private void AddGraves()
    {
      string[] graves =
      {
        "grave_1",
        "grave_2"
      };

      for (int x = 2; x < RoomWidth - 2; x++)
      {
        for (int y = 2; y < RoomHeight - 2; y++)
        {
          if (x % 3 == 1 && y % 2 == 0 && _random.NextDouble() < 0.7)
          {
            string name = graves[_random.Next(graves.Length)];
            AddProp(x, y, new Prop(name, true, Engine.Assets.GetAsset<Sprite>(name)));
          }
        }
      }

      if (_random.NextDouble() < 0.6)
      {
        AddBones();
      }
    }

    private void AddBones()
    {

      string[] bones =
      {
        "bones_1",
        "bones_2",
        "bones_3"
      };

      int boneCount = _random.Next(5, 10);
      for (int i = 0; i < boneCount; i++)
      {
        string name = bones[_random.Next(bones.Length)];
        Point2D point = GetRandomEmptyPoint(2);
        AddProp(point.X, point.Y, new Prop(name, false, Engine.Assets.GetAsset<Sprite>(name)));
      }
    }

    private void AddStatues()
    {
      int y = (_random.Next(4) * (RoomHeight / 4)) + 1;

      int mod = _random.Next(2, 4);

      for (int x = 2; x < RoomWidth - 2; x++)
      {
        if (x == RoomWidth / 2)
        {
          continue;
        }

        if (GetTile(x, y).IsSolid)
        {
          continue;
        }

        if (x % mod == (mod % 2 != 0 ? 1 : 0) && _random.NextDouble() < 0.8)
        {

          string[] statues =
          {
            "statue_1",
            "statue_2",
            "statue_3",
          };

          string name = statues[_random.Next(statues.Length)];

          AddProp(x, y, new Prop(name, true, Engine.Assets.GetAsset<Sprite>(name)));
        }
      }
    }

    private void AddBookcases()
    {
      for (int x = 2; x < RoomWidth - 2; x++)
      {
        for (int y = 1; y < RoomHeight - 2; y++)
        {
          if (y % 4 == 0 && x % 3 != 1 && _random.NextDouble() < 0.85)
          {
            string name = "bookcase_" + (_random.NextDouble() < 0.4 ? "empty" : "full");
            AddProp(x, y, new Prop(name, true, Engine.Assets.GetAsset<Sprite>(name)));
          }
        }
      }
    }

    private void Decorate()
    {
      if (_floorType.Equals("brick") && _random.NextDouble() < 0.3)
      {
        if (_random.NextDouble() < 0.6)
        {
          bool centeredFeature = _random.NextDouble() < 0.5;

          if (_random.NextDouble() < 0.7)
          {
            AddCarpet(2, 2, 3, 3, centeredFeature);
            AddCarpet(16, 2, 3, 3, centeredFeature);
            AddCarpet(2, 8, 3, 3, centeredFeature);
            AddCarpet(16, 8, 3, 3, centeredFeature);
          }

          if (_random.NextDouble() < 0.4)
          {
            AddCarpet(RoomWidth / 2 - 1, 1, 3, 11, false);
          }

        }
        else
        {
          AddCarpet(RoomWidth / 2 - 2, RoomHeight / 2 - 2, 5, 5, false);
        }

        if (_random.NextDouble() < 0.2)
        {
          AddEdgeProp(new[] { "urn_1", "urn_2", "urn_3" }, 0.35);
        }
      }

      if (_wallType.Equals("mossy"))
      {
        if (_random.NextDouble() < 0.45)
        {
          AddWebs();
        }

        if (_random.NextDouble() < 0.15)
        {
          AddAlters();
        }

        if (_random.NextDouble() < 0.5)
        {
          AddGraves();
        }
      }

      if (_floorType.Equals("cobble"))
      {
        if (_random.NextDouble() < 0.2)
        {
          AddEdgeProp(new[] { "stalagmite_1", "stalagmite_2" }, 0.1);
        }

        if (_random.NextDouble() < 0.2)
        {
          AddEdgeProp(new[] { "mushroom_1", "mushroom_2", "mushroom_3" }, 0.1);
        }
      }

      if (!_wallType.Equals("mossy"))
      {
        if (_random.NextDouble() < 0.5)
        {
          AddStatues();
        }

        if (!_floorType.Equals("cobble") && _random.NextDouble() < 0.2)
        {
          AddBookcases();
        }
      }

      for (int x = 2; x < RoomWidth - 2; x++)
      {
        for (int y = 2; y < RoomHeight - 2; y++)
        {
          Tile tile = GetTile(x, y);

          if (tile?.IsSolid ?? false)
          {
            continue;
          }

          Prop prop = GetProp(x, y);

          if (prop != null)
          {
            continue;
          }

          if (_random.NextDouble() < TreasureSpawnChance)
          {
            SpawnTreasure(x, y);
          }

          // Only spawn threats if we are not in the start room
          if (_random.NextDouble() < WeightedThreatSpawnChance && !_isStart)
          {
            SpawnThreat(x, y);
          }
        }
      }
    }

    private void AddStartItem()
    {
      if (!_isStart)
      {
        return;
      }

      Point2D spawn = GetRandomEmptyPoint(2);
      Item item = ItemGenerator.GetSpawnItem(_level);

      if (item == null)
      {
        Console.WriteLine($"Level {_level} has no spawn item.");
        return;
      }

      AddItem(spawn.X, spawn.Y, item);
    }

    private void SpawnTreasure(int x, int y)
    {
      Item item = ItemGenerator.RandomTreasure(_random);
      AddItem(x, y, item);
    }

    private void SpawnThreat(int x, int y)
    {
      Creature creature = CreatureGenerator.GetThreatForLevel(_level, _random);

      if (creature == null)
      {
        Console.WriteLine("Cannot spawn creature for level " + _level);
        return;
      }

      new MonsterAi(creature);
      creature.CurrentLevel = _level;
      creature.X = x;
      creature.Y = y;
      _creatures.Add(creature);
    }

    private Point2D GetRandomEmptyPoint(int padding = 0)
    {
      Point2D point;
      do
      {

        point = new Point2D(_random.Next(padding, RoomWidth - padding), _random.Next(padding, RoomHeight - padding));
      } while (_items.ContainsKey(point) || _tiles[point].IsSolid || (GetProp(point.X, point.Y)?.IsSolid ?? false));

      return point;
    }

    private void AddItem(int x, int y, Item item)
    {
      _items.Add(new Point2D(x, y), item);
      item.X = x;
      item.Y = y;
    }

    private void AddProp(int x, int y, Prop prop)
    {
      var point = new Point2D(x, y);
      if (_props.ContainsKey(point) || GetTile(x, y).IsSolid)
      {
        return;
      }
      _props.Add(point, prop);
      prop.X = x;
      prop.Y = y;
    }

    private Prop GetProp(int x, int y) => _props.ContainsKey(new Point2D(x, y)) ? _props[new Point2D(x, y)] : null;

    private void SetTile(int x, int y, Tile tile)
    {
      if (_tiles.InBounds(x, y))
      {
        _tiles[x, y] = tile;
      }
    }

    private Tile GetTile(int x, int y) => _tiles.InBounds(x, y) ? _tiles[x, y] : null;
  }
}
