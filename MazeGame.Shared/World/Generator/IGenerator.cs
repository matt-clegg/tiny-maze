﻿namespace MazeGame.World.Generator
{
  public interface IGenerator<T> where T : class
  {
    T Build();
    IGenerator<T> Generate();
  }
}
