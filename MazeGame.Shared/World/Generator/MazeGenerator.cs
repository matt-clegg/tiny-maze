﻿using MazeGame.Util;
using System;
using System.Collections.Generic;

namespace MazeGame.World.Generator
{
  public class MazeGenerator : IGenerator<Maze>
  {
    private readonly IRandom _random;

    private readonly Config _worldConfig;
    private Level[] _levels;

    public MazeGenerator(Config worldConfig, IRandom random = null)
    {
      _worldConfig = worldConfig;
      _random = random ?? new GameRandom();
    }

    private int Levels => _worldConfig.GetInt("levels");

    public Maze Build()
    {
      return new Maze(_levels);
    }

    public IGenerator<Maze> Generate()
    {
      _levels = new Level[Levels];

      for (int i = 0; i < _levels.Length; i++)
      {
        ChooseLevelType(out string wallType, out string floorType);

        _levels[i] = new LevelGenerator(_worldConfig, i, wallType, floorType, _random)
                    .Generate()
                    .Build();
      }

      return this;
    }

    private void ChooseLevelType(out string wallType, out string floorType)
    {
      var choices = new List<Tuple<string, string>>
      {
        Tuple.Create( "mossy", "mossy_brick" ),
        Tuple.Create( "stone", "cobble" ),
        Tuple.Create( "stone", "brick" ),
      };

      Tuple<string, string> choice = choices[_random.Next(choices.Count)];
      wallType = choice.Item1;
      floorType = choice.Item2;
    }

  }
}
