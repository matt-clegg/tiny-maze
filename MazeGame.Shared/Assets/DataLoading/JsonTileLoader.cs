﻿using System.Collections.Generic;
using MazeGame.Graphics;
using MazeGame.World;
using Microsoft.Xna.Framework.Content;
using Newtonsoft.Json;

namespace MazeGame.Assets.DataLoading
{
  public class JsonTileLoader<TData> : JsonLoader<TData> where TData : TileData
  {
    public JsonTileLoader(AssetCache<string> assets, ContentManager content) : base(assets, content)
    {
    }

    public override void Load(string jsonData)
    {
      List<TileData> tileData = JsonConvert.DeserializeObject<List<TileData>>(jsonData);

      foreach (TileData data in tileData)
      {
        Sprite sprite = Assets.GetAsset<Sprite>(data.Sprite ?? data.Name);
        Assets.AddAsset(data.Name, new Tile(data.Name, sprite, data.IsSolid));
      }
    }
  }
}