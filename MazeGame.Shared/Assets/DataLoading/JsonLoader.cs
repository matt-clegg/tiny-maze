﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Newtonsoft.Json;

namespace MazeGame.Assets.DataLoading
{
  public class JsonLoader<TData> where TData : INamedJsonItem
  {
    protected AssetCache<string> Assets { get; }
    protected ContentManager Content { get; }

    public JsonLoader(AssetCache<string> assets, ContentManager content)
    {
      Assets = assets;
      Content = content;
    }

    public virtual void Load(string jsonData)
    {
      List<TData> data = JsonConvert.DeserializeObject<List<TData>>(jsonData);
      foreach (TData item in data) {
        Assets.AddAsset(item.Name, item);
      }
    }
  }
}
