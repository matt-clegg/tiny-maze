﻿using System.Collections.Generic;
using MazeGame.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Newtonsoft.Json.Linq;

namespace MazeGame.Assets.DataLoading
{
  public class JsonSpriteLoader<TData> : JsonLoader<TData> where TData : SpriteData
  {
    public JsonSpriteLoader(AssetCache<string> assets, ContentManager content) : base(assets, content)
    {
    }

    public override void Load(string jsonData)
    {
      JObject root = JObject.Parse(jsonData);

      int? defaultWidth = root["defaultWidth"].Value<int>();
      int? defaultHeight = root["defaultHeight"].Value<int>();

      List<SpriteData> spriteData = root["sprites"].Value<JArray>().ToObject<List<SpriteData>>();

      foreach (SpriteData data in spriteData)
      {
        Spritesheet sheet = Assets.GetAsset<Spritesheet>(data.Sheet);

        int width = data.Width ?? defaultWidth.Value;
        int height = data.Height ?? defaultHeight.Value;

        if (data.IsMonster)
        {
          LoadMonsterSprites(data, width, height, sheet);
        }
        else
        {
          AddSprite(data.Name, data.X, data.Y, width, height, sheet);
        }
      }
    }

    private void LoadMonsterSprites(SpriteData data, int width, int height, Spritesheet sheet)
    {
      AddSprite($"{data.Name}_right_1", data.X, data.Y, width, height, sheet);
      AddSprite($"{data.Name}_right_2", data.X, data.Y + 1, width, height, sheet);

      AddSprite($"{data.Name}_bottom_1", data.X + 1, data.Y, width, height, sheet);
      AddSprite($"{data.Name}_bottom_2", data.X + 1, data.Y + 1, width, height, sheet);

      AddSprite($"{data.Name}_top_1", data.X + 2, data.Y, width, height, sheet);
      AddSprite($"{data.Name}_top_2", data.X + 2, data.Y + 1, width, height, sheet);

      AddSprite($"{data.Name}_left_1", data.X + 3, data.Y, width, height, sheet);
      AddSprite($"{data.Name}_left_2", data.X + 3, data.Y + 1, width, height, sheet);
    }

    private void AddSprite(string name, int x, int y, int width, int height, Spritesheet sheet)
    {
      var bounds = new Rectangle(x * width, y * height, width, height);
      Assets.AddAsset(name, new Sprite(name, sheet.Texture, bounds));
    }
  }
}
