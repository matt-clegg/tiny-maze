﻿namespace MazeGame.Assets.DataLoading
{
  public class SpriteData : INamedJsonItem
  {
    public string Sheet { get; }
    public string Name { get; }
    public int X { get; }
    public int Y { get; }


    public int? Width { get; }
    public int? Height { get; }

    public bool IsMonster { get; }

    public SpriteData(string sheet, string name, int x, int y, int? width, int? height, bool isMonster = false)
    {
      Sheet = sheet;
      Name = name;
      X = x;
      Y = y;
      Width = width;
      Height = height;
      IsMonster = isMonster;
    }
  }
}
