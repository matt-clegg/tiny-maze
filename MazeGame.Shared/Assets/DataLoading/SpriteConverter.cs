﻿using System;
using MazeGame.Graphics;
using Newtonsoft.Json;

namespace MazeGame.Assets.DataLoading
{
  public class SpriteConverter : JsonConverter
  {
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
      throw new NotImplementedException();
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
      if (reader.TokenType == JsonToken.String)
      {
        string name = (reader.Value as string)?.Trim();

        if (Engine.Assets.Has<Sprite>(name))
        {
          return Engine.Assets.GetAsset<Sprite>(name);
        }

        throw new JsonSerializationException($"Unknown sprite: '{name}'");

      }
      throw new JsonSerializationException("Sprite property must be a name.");
    }

    public override bool CanConvert(Type objectType) => false;
    public override bool CanWrite => false;
  }
}
