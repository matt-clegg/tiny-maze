﻿using System;
using System.IO;
using MazeGame.Entities.Creatures;
using MazeGame.Entities.Items;
using MazeGame.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MazeGame.Assets.DataLoading
{
  public class AssetLoader
  {
    private readonly AssetCache<string> _assets;
    private readonly ContentManager _content;

    public AssetLoader(AssetCache<string> assets, ContentManager content)
    {
      _assets = assets;
      _content = content;
    }

    public void Load(string filePath)
    {
      if (!File.Exists(filePath))
      {
        throw new FileNotFoundException($"{filePath} not found.");
      }

      string name = Path.GetFileNameWithoutExtension(filePath);

      string jsonData = File.ReadAllText( filePath );

      switch ( name ) {
        case "spritesheets":
          new JsonSpritesheetLoader<SpritesheetData>( _assets, _content ).Load( jsonData );
          break;
        case "sprites":
          new JsonSpriteLoader<SpriteData>(_assets, _content).Load(jsonData);
          break;
        case "creatures":
          new JsonLoader<CreatureType>( _assets, _content ).Load( jsonData );
          break;
        case "items":
          new JsonLoader<ItemType>( _assets, _content ).Load( jsonData );
          break;
        case "tiles":
          new JsonTileLoader<TileData>( _assets, _content ).Load( jsonData );
          break;
        default:
          Console.WriteLine( $"Unknown asset file type: {name}" );
          break;
      }
    }

    public void LoadFont()
    {
      const string Charset = "abcdefghijklmnopqrstuvwxyz0123456789" +
                             "!@#$%^&*()-+=:;,\"<>.?/\\[]_|?????????";

      Spritesheet fontSheet = new Spritesheet("font_spritesheet", _content.Load<Texture2D>("Textures/tiny_dungeon_font"));
      var font = new Font("font", fontSheet.Texture, 6, 7, Charset);
      _assets.AddAsset("font", font);
    }

  }
}
