﻿namespace MazeGame.Assets.DataLoading
{
  public interface INamedJsonItem
  {
    string Name { get; }
  }
}
