﻿using System.Collections.Generic;
using MazeGame.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;

namespace MazeGame.Assets.DataLoading
{
  public class JsonSpritesheetLoader<TData> : JsonLoader<TData> where TData : SpritesheetData
  {
    public JsonSpritesheetLoader(AssetCache<string> assets, ContentManager content) : base(assets, content)
    {
    }

    public override void Load(string jsonData)
    {
      List<string> spritesheetData = JsonConvert.DeserializeObject<List<string>>(jsonData);

      foreach (string name in spritesheetData)
      {
        Texture2D texture = Content.Load<Texture2D>("Textures/" + name);
        Assets.AddAsset(name, new Spritesheet(name, texture));
      }
    }
  }
}
