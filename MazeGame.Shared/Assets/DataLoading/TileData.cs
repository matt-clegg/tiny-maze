﻿namespace MazeGame.Assets.DataLoading
{
  public class TileData : INamedJsonItem
  {
    public string Name { get; set; }
    public string Sprite { get; set; }
    public bool IsSolid { get; set; }
  }
}
