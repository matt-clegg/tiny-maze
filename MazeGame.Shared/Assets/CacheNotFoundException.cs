﻿using System;

namespace MazeGame.Assets
{
  public class CacheNotFoundException : Exception
  {
    public CacheNotFoundException(string message) : base(message)
    {

    }
  }
}
