﻿using MazeGame.Action;
using MazeGame.Entities.Creatures;
using MazeGame.Entities.Props;
using MazeGame.World;

namespace MazeGame.Ai
{
  public class PlayerAi : CreatureAi
  {
    public PlayerAi(Creature creature) : base(creature)
    {
    }

    public override IAction DecideNextAction() => null;

    public override bool OnEnter(int x, int y, Tile tile, Prop prop)
    {
      Room oldRoom = Creature.CurrentRoom;
      bool hasMoved = base.OnEnter(x, y, tile, prop);

      if (hasMoved && Creature.CurrentRoom.Position == oldRoom.Position)
      {
        Creature.CurrentRoom.AddFootprintAtTile(Creature.X, Creature.Y);
      }

      return hasMoved;
    }
  }
}
