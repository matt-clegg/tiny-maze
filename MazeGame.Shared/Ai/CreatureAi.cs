﻿using MazeGame.Action;
using MazeGame.Entities.Creatures;
using MazeGame.Entities.Props;
using MazeGame.World;

namespace MazeGame.Ai
{
  public abstract class CreatureAi
  {
    protected Creature Creature { get; }

    public CreatureAi(Creature creature)
    {
      Creature = creature;
      Creature.Ai = this;
    }

    public abstract IAction DecideNextAction();

    public virtual bool OnEnter(int x, int y, Tile tile, Prop prop)
    {
      if (tile.IsSolid)
      {
        return false;
      }

      if (prop is Door door)
      {
        if (!door.IsOpen || !Creature.CanChangeRooms)
        {
          return false;
        }

        Creature.TryEnterRoomAt(x, y);
        return true;
      }

      if (prop?.IsSolid ?? false)
      {
        return false;
      }

      Creature.X = x;
      Creature.Y = y;
      return true;
    }
  }
}
