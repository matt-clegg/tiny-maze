﻿using MazeGame.Action;
using MazeGame.Entities.Creatures;
using System;

namespace MazeGame.Ai
{
  public class MonsterAi : CreatureAi
  {
    private Player _player;

    public MonsterAi(Creature creature) : base(creature)
    {
    }

    public override IAction DecideNextAction()
    {
      if (_player == null)
      {
        FindPlayer();
        return new MoveAction( 0, 0 );
      }

      return FollowPlayerAction();
    }

    private void FindPlayer()
    {
      foreach (Creature creature in Creature.CurrentRoom.Creatures)
      {
        if (creature is Player player)
        {
          _player = player;
        }
      }
    }

    private IAction FollowPlayerAction()
    {
      int x = 0;
      int y = 0;
      
      if (Math.Abs(_player.Y - Creature.Y) == 0)
      {
        x = _player.X > Creature.X ? 1 : -1;
      }
      else
      {
        y = _player.Y > Creature.Y ? 1 : -1;
      }

      return new MoveAction(x, y);
    }
   
  }
}
