﻿using MazeGame.Entities.Creatures;
using MazeGame.Turns;

namespace MazeGame.Action
{
  public class ChangeItemAction : IAction
  {
    private readonly int _indexChange;
    private readonly int? _newIndex;

    public ChangeItemAction(int? indexChange = null, int? newIndex = null)
    {
      _indexChange = indexChange ?? 0;
      _newIndex = newIndex;
    }

    public ActionResult Perform(ITurnable turnable, TurnResult result)
    {
      if (turnable is Player player)
      {

        if (_newIndex < 0 || _newIndex >= player.ActionItems.Count)
        {
          return ActionResult.Failure;
        }

        player.SelectItem(_newIndex ?? (player.SelectedItemIndex + _indexChange));
        return ActionResult.Success;
      }

      return ActionResult.Failure;
    }
  }
}
