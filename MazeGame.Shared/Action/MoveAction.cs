﻿using MazeGame.Entities.Creatures;
using MazeGame.Entities.Items;
using MazeGame.Turns;

namespace MazeGame.Action
{
  public class MoveAction : IAction
  {
    private readonly int _x;
    private readonly int _y;

    public MoveAction(int x, int y)
    {
      _x = x;
      _y = y;
    }

    public ActionResult Perform(ITurnable turnable, TurnResult result)
    {
      Creature creature = turnable as Creature;

      if (creature == null)
      {
        return ActionResult.Failure;
      }

      int newX = creature.X + _x;
      int newY = creature.Y + _y;

      Creature other = creature.CurrentRoom.GetCreature(newX, newY);
      if (other is Player otherPlayer)
      {
        return new ActionResult(new TakeWealthAction(otherPlayer));
      }

      if (creature is Player && other != null)
      {
        return new ActionResult(new AttackAction(other));
      }

      if ( other != null ) {
        return ActionResult.Failure;
      }

      bool hasMoved = creature.TryMoveBy(_x, _y);

      if (hasMoved)
      {
        Player player = creature as Player;
        Item item = player?.CurrentRoom.GetItem(player.X, player.Y);

        if (item != null)
        {
          return new ActionResult(new PickupAction(player.X, player.Y));
        }
      }

      return hasMoved ? ActionResult.Success : ActionResult.Failure;
    }
  }
}
