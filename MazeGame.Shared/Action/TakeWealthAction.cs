﻿using MazeGame.Entities.Creatures;
using MazeGame.GameEvents;
using MazeGame.Turns;
using Microsoft.Xna.Framework;

namespace MazeGame.Action
{
  public class TakeWealthAction : IAction
  {
    private readonly Player _player;

    public TakeWealthAction(Player player)
    {
      _player = player;
    }

    public ActionResult Perform(ITurnable turnable, TurnResult result)
    {
      if (_player == null)
      {
        return ActionResult.Failure;
      }

      Creature attacker = turnable as Creature;

      int wealthDamage = attacker?.WealthDamage ?? 0;

      _player.Wealth = _player.Wealth - wealthDamage;

      if (_player.Wealth > 0)
      {
        string text = $"-{wealthDamage}";
        var effect = new TextEffect(_player.X, _player.Y, text, Color.Red)
        {
          RenderOrder = -5
        };
        result.AddEvent(new GameEvent(effect));
      }
      return ActionResult.Success;

    }
  }
}
