﻿using MazeGame.Turns;

namespace MazeGame.Action
{
  public interface IAction
  {
    ActionResult Perform(ITurnable turnable, TurnResult result);
  }
}
