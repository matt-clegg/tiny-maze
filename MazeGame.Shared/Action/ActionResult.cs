﻿namespace MazeGame.Action
{
  public class ActionResult
  {
    public static readonly ActionResult Success = new ActionResult(true);
    public static readonly ActionResult Failure = new ActionResult(false);

    public bool Succeeded { get; }
    public IAction Alternative { get; }

    public ActionResult(bool succeeded)
    {
      Succeeded = succeeded;
      Alternative = null;
    }

    public ActionResult(IAction alternative)
    {
      Alternative = alternative;
      Succeeded = false;
    }
  }
}
