﻿using MazeGame.Entities.Creatures;
using MazeGame.Entities.Items;
using MazeGame.GameEvents;
using MazeGame.Turns;
using Microsoft.Xna.Framework;

namespace MazeGame.Action
{
  public class PickupAction : IAction
  {
    private readonly int _x;
    private readonly int _y;

    public PickupAction(int x, int y)
    {
      _x = x;
      _y = y;
    }

    public ActionResult Perform(ITurnable turnable, TurnResult result)
    {
      Player player = turnable as Player;
      Item item = player?.CurrentRoom.GetItem(_x, _y);

      if (item != null)
      {
        player.Wealth += item.Value;
        player.CurrentRoom.RemoveItem(item);

        string pickupText = $"+{(item.Value > 0 ? item.Value.ToString() : item.Name)}";
        result.AddEvent(new GameEvent(new TextEffect(_x, _y, pickupText, Color.GreenYellow)));

        if (!string.IsNullOrWhiteSpace(item.Action))
        {
          string messageText = $"You have learned \"{item.Action}\"!";
          result.AddEvent(new GameEvent(new MessageEffect(messageText, Color.White)));
          player.LearnAction(item);
        }

        return ActionResult.Success;
      }

      return ActionResult.Failure;
    }
  }
}
