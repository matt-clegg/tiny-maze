﻿using MazeGame.Entities.Creatures;
using MazeGame.Entities.Props;
using MazeGame.GameEvents;
using MazeGame.Turns;
using Microsoft.Xna.Framework;

namespace MazeGame.Action
{
  public class UseStairs : IAction
  {
    private readonly int _x;
    private readonly int _y;

    public UseStairs(int x, int y)
    {
      _x = x;
      _y = y;
    }

    public ActionResult Perform(ITurnable turnable, TurnResult result)
    {
      if (turnable is Player player)
      {
        Prop prop = player.CurrentRoom.GetProp(_x, _y);

        if (prop == null)
        {
          return ActionResult.Failure;
        }

        if (prop.Name.Equals("stairs_down"))
        {
          player.ChangeLevel(player.CurrentLevel + 1);
          if (!player.CurrentRoom.IsEnd)
          {
            result.AddEvent(new GameEvent(new MessageEffect("Level " + (player.CurrentLevel + 1), Color.White)));
          }

          return ActionResult.Success;
        }

        if (prop.Name.Equals("stairs_up"))
        {
          result.AddEvent(new GameEvent(new TextEffect(_x, _y, "Nope", Color.Red)));
          return ActionResult.Failure;
        }
      }

      return ActionResult.Failure;
    }

  }
}
