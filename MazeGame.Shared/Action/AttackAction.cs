﻿using MazeGame.Entities.Creatures;
using MazeGame.Entities.Items;
using MazeGame.GameEvents;
using MazeGame.Turns;
using Microsoft.Xna.Framework;

namespace MazeGame.Action
{
  public class AttackAction : IAction
  {
    private readonly Creature _other;

    public AttackAction(Creature other)
    {
      _other = other;
    }

    public ActionResult Perform(ITurnable turnable, TurnResult result)
    {
      Player player = turnable as Player;
      if (player == null)
      {
        return ActionResult.Failure;
      }

      Item actionItem = player.SelectedItem;

      if (_other.Attack(actionItem)) {
        var effect =new TextEffect( player.X, player.Y, actionItem.Action + "!", Color.GreenYellow )
        {
          RenderOrder = 10
        };
        result.AddEvent(new GameEvent(effect));
        return ActionResult.Success;
      }
      else {
        var effect = new TextEffect( player.X, player.Y, "Nothing happens", Color.Orange )
        {
          RenderOrder = 10
        };
        result.AddEvent(new GameEvent(effect));
        return ActionResult.Failure;
      }

    }
  }
}
