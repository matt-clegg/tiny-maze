﻿namespace MazeGame.GameEvents
{
  public class GameEvent
  {
    public Effect Effect { get; }
    
    public GameEvent( Effect effect)
    {
      Effect = effect;
    }
  }
}
