﻿using MazeGame.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MazeGame.GameEvents
{
  public class DripEffect : Effect
  {
    private readonly AnimatedSprite _dripAnimation;

    private double _offsetY;
    private double _offsetSpeedY;

    public DripEffect(int x, int y) : base(x, y, 0)
    {
      // The first frame is a tiny square, the rest of the drip animation plays when the drip hits the floor
      _dripAnimation = new AnimatedSprite(100, 0, Engine.Assets.GetAsset<Sprite>("drop"),
                                                  Engine.Assets.GetAsset<Sprite>("drip_1"),
                                                  Engine.Assets.GetAsset<Sprite>("drip_2"),
                                                  Engine.Assets.GetAsset<Sprite>("drip_3"))
      {
        IsLooped = false,
        IsRunning = false
      };

      _offsetY = 20;
      _offsetSpeedY = 0.2;
    }

    public override bool Update(GameTime gameTime)
    {
      _offsetY -= _offsetSpeedY;
      _offsetSpeedY *= 1.2;

      if (_offsetY <= 0)
      {
        _dripAnimation.IsRunning = true;
        _offsetY = 0;
      }

      _dripAnimation.Update(gameTime);
      return _offsetY <= 0 && !_dripAnimation.IsRunning;
    }

    public override void Draw(SpriteBatch batch)
    {
      DrawSprite(_dripAnimation?.GetSprite, X, Y, Color.DeepSkyBlue, batch, offsetY: (int)-_offsetY);
    }
  }
}
