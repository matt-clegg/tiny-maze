﻿using MazeGame.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MazeGame.GameEvents
{
  public class MessageEffect : Effect
  {
    private const double OffsetDampen = 0.8;

    private readonly string _message;
    private readonly Color _color;

    private readonly Font _font;

    private readonly int _x;
    private readonly int _y;

    private double _offsetY;
    private double _offsetYSpeed;

    public MessageEffect(string message, Color color) : base( 0, 0, 100 )
    {
      _message = message;
      _color = color;
      _font = Engine.Assets.GetAsset<Font>( "font" );

      _x = Engine.VirtualWidth / 2;
      _y = (int) (Engine.VirtualHeight * 0.4);

      _x -= (_message.Length / 2) * _font.CharWidth;

      _offsetYSpeed = 4;
    }

    public override bool Update(GameTime gameTime)
    {
      _offsetY += _offsetYSpeed;
      _offsetYSpeed *= OffsetDampen;

      return base.Update(gameTime);
    }

    public override void Draw( SpriteBatch batch )
    {
      _font.WritePrecise( _message, _x, (int) (_y - _offsetY), _color, batch );
    }
  }
}
