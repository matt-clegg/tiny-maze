﻿using MazeGame.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MazeGame.GameEvents
{
  public abstract class Effect
  {
    protected int X { get; }
    protected int Y { get; }
    public int RenderOrder { get; set; }

    private int _life;

    public Effect( int x, int y, int life )
    {
      X = x;
      Y = y;
      _life = life;
    }

    public virtual bool Update(GameTime gameTime)
    {
      return --_life < 0;
    }

    public abstract void Draw( SpriteBatch batch );

    protected void DrawSprite(Sprite sprite, int x, int y, Color color, SpriteBatch batch, int offsetX = 0, int offsetY = 0)
    {
      batch.Draw(sprite.Texture, new Rectangle(x * sprite.Width + offsetX, y * sprite.Height + offsetY, sprite.Width, sprite.Height), sprite.Bounds, color);
    }
  }
}
