﻿using System;
using MazeGame.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MazeGame.GameEvents
{
  public class TextEffect : Effect
  {
    private const int TileSize = 16;
    private const double OffsetSpeed = 5;
    private const double OffsetDampen = 0.8;
    private const double EndOffsetThreshold = 0.0005;

    private readonly Font _font;

    private readonly string _text;
    private readonly Color _color;

    private double _offsetX;
    private double _offsetY;

    private double _offsetXSpeed;
    private double _offsetYSpeed;

    public TextEffect(int x, int y, string text, Color color) : base(x, y, 0)
    {
      _font = Engine.Assets.GetAsset<Font>("font");
      _offsetYSpeed = OffsetSpeed;
      _text = text;
      _color = color;

      _offsetYSpeed *= Engine.Instance.Random.NextDouble() * 0.3 + 0.8;
      _offsetXSpeed = Engine.Instance.Random.NextDouble(-1.0, 2.0);
    }

    public override bool Update(GameTime gameTime)
    {
      _offsetX += _offsetXSpeed;
      _offsetY += _offsetYSpeed;

      _offsetXSpeed *= OffsetDampen;
      _offsetYSpeed *= OffsetDampen;
      
      return Math.Abs(_offsetYSpeed) < EndOffsetThreshold;
    }

    public override void Draw(SpriteBatch batch)
    {
      int x = X * TileSize - (_text.Length * _font.CharWidth) / 4 + (int)_offsetX;
      int y = Y * TileSize - (int)_offsetY;

      if (y < 0)
      {
        y = 0;
      }

      _font.WritePrecise(_text, x, y, _color, batch);
    }
  }
}
