﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MazeGame.Scenes
{
  public abstract class Scene
  {
    public abstract void Update(GameTime gameTime);
    public abstract void Draw(SpriteBatch batch);
    public abstract void Input(Keys key);
  }
}
