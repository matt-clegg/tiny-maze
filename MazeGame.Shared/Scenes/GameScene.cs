﻿using MazeGame.Ai;
using MazeGame.Entities.Creatures;
using MazeGame.Entities.Items;
using MazeGame.GameEvents;
using MazeGame.Graphics;
using MazeGame.Graphics.Rendering;
using MazeGame.Turns;
using MazeGame.Util;
using MazeGame.World;
using MazeGame.World.Generator;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Effect = MazeGame.GameEvents.Effect;

namespace MazeGame.Scenes
{
  public class GameScene : Scene
  {
    private readonly List<Renderer> _renderers = new List<Renderer>();
    private readonly RoomRenderer _roomRenderer;

    private readonly Config _worldConfig;
    private readonly Player _player;
    private Maze _maze;

    private readonly List<Effect> _effects = new List<Effect>();
    private readonly List<Effect> _effectsToRemove = new List<Effect>();

    public GameScene()
    {
      _worldConfig = ConfigParser.Load($"{Engine.Instance.ContentLocation}\\Data\\world.txt");

      _player = new Player(_worldConfig.GetInt("player_speed"), "player");
      _player.RestartTriggerEvent += OnRestart;
      _player.RoomChangedEvent += OnRoomChange;
      new PlayerAi(_player);

      for (int i = 0; i < 6; i++)
      {
        Item item = ItemGenerator.GetSpawnItem(i);
        //_player.LearnAction(item);
      }

      Font font = Engine.Assets.GetAsset<Font>("font");

      _roomRenderer = new RoomRenderer();
      _renderers.Add(_roomRenderer);
      _renderers.Add(new EffectsRenderer(_effects));
      _renderers.Add(new InterfaceRenderer(font, _player));

      NewGame();
    }

    private Room CurrentRoom => _maze.GetRoom(_player.CurrentRoomPosition, _player.CurrentLevel);

    private void OnRestart(object sender, EventArgs e) => NewGame();

    public void NewGame()
    {
      const int seed = 123;
      IRandom random = new GameRandom();

      _maze = new MazeGenerator(_worldConfig, random)
              .Generate()
              .Build();

      _maze.Init();

      Room spawnRoom = _maze.GetLevel(0).StartingRoom;

      spawnRoom.AddCreature(_player, spawnRoom.Width / 2, spawnRoom.Height / 2);
      _player.Init(_maze, spawnRoom.Position);

      _roomRenderer.Room = spawnRoom;

      Timer.Start();
    }

    private void OnRoomChange(object sender, EventArgs args)
    {
      if (args is RoomChangeEventArgs roomArgs)
      {
        _roomRenderer.Room = roomArgs.NewRoom;
        if (roomArgs.OldRoom.Level == roomArgs.NewRoom.Level)
        {
          roomArgs.NewRoom.OpenPassageTo(roomArgs.OldRoom);
          roomArgs.OldRoom.OpenPassageTo(roomArgs.NewRoom);
        }
      }
    }

    private void UpdateEffects(GameTime gameTime)
    {
      foreach (Effect effect in _effects)
      {
        if (effect.Update(gameTime))
        {
          _effectsToRemove.Add(effect);
        }
      }
    }

    public override void Update(GameTime gameTime)
    {
      UpdateEffects(gameTime);

      foreach (Effect effect in _effectsToRemove)
      {
        _effects.Remove(effect);
      }
      _effectsToRemove.Clear();

      int level = _player.CurrentLevel;
      TurnResult result = CurrentRoom.Update(gameTime);

      if (result.NeedsRefresh)
      {
        _maze.UpdateFootprints(level);
      }

      foreach (GameEvent gameEvent in result.Events)
      {
        _effects.Add(gameEvent.Effect);
      }

      _maze.Update(level);

      foreach (Renderer renderer in _renderers)
      {
        renderer.Update();
      }
    }

    public override void Draw(SpriteBatch batch)
    {
      foreach (Renderer renderer in _renderers)
      {
        renderer.Draw(batch);
      }
    }

    public override void Input(Keys key)
    {
      _player.Input(key);
    }

  }
}
