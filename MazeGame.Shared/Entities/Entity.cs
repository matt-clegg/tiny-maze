﻿using MazeGame.Graphics;
using MazeGame.Util;
using MazeGame.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MazeGame.Entities
{
  public abstract class Entity
  {
    public int X { get; set; }
    public int Y { get; set; }

    public string Name { get; set; }
    public virtual Sprite Sprite { get; }

    protected Maze Maze { get; private set; }
    public Point2D CurrentRoomPosition { get; set; }
    public int CurrentLevel { get; set; }

    public Room CurrentRoom => Maze.GetRoom(CurrentRoomPosition, CurrentLevel);

    public bool ShouldRemove { get; set; }

    public Entity(string name, Sprite sprite)
    {
      Name = name;
      Sprite = sprite;
    }

    public void Init(Maze maze, Point2D currentRoomPosition)
    {
      Maze = maze;
      CurrentRoomPosition = currentRoomPosition;
    }

    public virtual void Draw(SpriteBatch batch)
    {
      DrawSprite(Sprite, X, Y, Color.White, batch);
    }

    protected void DrawSprite(Sprite sprite, int x, int y, Color color, SpriteBatch batch, int offsetX = 0, int offsetY = 0)
    {
      batch.Draw(sprite.Texture, new Rectangle(x * sprite.Width + offsetX, y * sprite.Height + offsetY, sprite.Width, sprite.Height), sprite.Bounds, color);
    }
  }
}
