﻿using MazeGame.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace MazeGame.Entities
{
  public class AnimationSet<T>
  {
    private readonly Dictionary<T, AnimatedSprite> _sprites = new Dictionary<T, AnimatedSprite>();

    public AnimatedSprite CurrentAnimation { get; private set; }

    public void Update(GameTime gameTime)
    {
      CurrentAnimation?.Update(gameTime);
    }

    public void SetKey(T key)
    {
      CurrentAnimation = _sprites[key];
    }

    /// <summary>
    /// Add a new animation that triggers when the specified key is set using the <see cref="SetKey"/> method.
    /// </summary>
    /// <param name="key"></param>
    /// <param name="animatedSprite"></param>
    /// <returns></returns>
    public AnimationSet<T> AddAnimation(T key, AnimatedSprite animatedSprite)
    {
      _sprites.Add(key, animatedSprite);
      if (_sprites.Count == 1)
      {
        CurrentAnimation = animatedSprite;
      }

      return this;
    }

  }
}
