﻿using MazeGame.Extensions;
using MazeGame.Graphics;
using MazeGame.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace MazeGame.Entities.Items
{
  public class Item : Entity
  {
    private const double GlintChance = 0.007;
    private const double GlintStartChance = 0.1;
    private const int GlintRenderOffset = 4;

    public int Value { get; private set; }
    private readonly string _valueRange;

    public string Action { get; }
    public int SpawnLevel { get; }

    private AnimatedSprite _glint;

    private int _glintOffsetX;
    private int _glintOffsetY;

    private Item(string name, Sprite sprite, string valueRange, string action, int spawnLevel) : base(name, sprite)
    {
      _valueRange = valueRange;
      Action = action;
      SpawnLevel = spawnLevel;
    }

    public void Update(GameTime gameTime)
    {
      if (_glint == null)
      {
        return;
      }

      _glint.Update(gameTime);

      if (!_glint.IsRunning && Engine.Instance.Random.NextDouble() < GlintChance)
      {
        SetGlintOffset(GlintRenderOffset);
        _glint.IsRunning = true;
      }
    }

    /// <summary>
    /// When called, this method creates a new glint animation that appears over the item.
    /// </summary>
    private void EnableGlint()
    {
      string glintType = Engine.Instance.Random.NextDouble() < 0.5 ? "a" : "b";

      _glint = new AnimatedSprite(150, 2, Engine.Assets.GetAsset<Sprite>($"glint_fx_{glintType}_1"),
                                   Engine.Assets.GetAsset<Sprite>($"glint_fx_{glintType}_2"),
                                   Engine.Assets.GetAsset<Sprite>($"glint_fx_{glintType}_3"))
      {
        IsLooped = false,
        IsRunning = Engine.Instance.Random.NextDouble() < GlintStartChance
      };

      SetGlintOffset(GlintRenderOffset);
    }

    private void SetGlintOffset(int offset)
    {
      _glintOffsetX = Engine.Instance.Random.Next(-offset, offset);
      _glintOffsetY = Engine.Instance.Random.Next(-offset, offset);
    }

    public override void Draw(SpriteBatch batch)
    {
      base.Draw(batch);

      if (_glint?.IsRunning ?? false)
      {
        DrawSprite(_glint?.GetSprite, X, Y, Color.White, batch, _glintOffsetX, _glintOffsetY);
      }
    }

    public void SetItemValue(IRandom random)
    {
      if (string.IsNullOrWhiteSpace(_valueRange))
      {
        Console.WriteLine($"Item {Name} has no value.");
        return;
      }

      Value = _valueRange.NumberFromRange(random);

      EnableGlint();
    }

    /// <summary>
    /// Creates a new Item instance from a specified <see cref="ItemType"/>.
    /// </summary>
    /// <param name="type"></param>
    /// <returns>The new Item instance.</returns>
    public static Item FromType(ItemType type)
    {
      return new Item(type.Name, type.Sprite, type.Value, type.Action, type.SpawnLevel);
    }
  }
}
