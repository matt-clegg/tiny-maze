﻿using MazeGame.Util;
using System.Collections.Generic;
using System.Linq;

namespace MazeGame.Entities.Items
{
  public static class ItemGenerator
  {

    public static Item RandomTreasure(IRandom random)
    {
      IEnumerable<ItemType> items = GetItemTypes();

      var weighted = new WeightedList<ItemType>(random);

      foreach (ItemType type in items.Where(type => !string.IsNullOrWhiteSpace(type.Value)))
      {
        weighted.AddItem(type, type.SpawnWeight);
      }

      Item item = Item.FromType(weighted.Get());
      item.SetItemValue(random);

      return item;
    }

    public static Item GetSpawnItem(int level)
    {
      ItemType itemType = GetItemTypes().FirstOrDefault(type => type.SpawnLevel - 1 == level);
      return itemType != null ? Item.FromType(itemType) : null;
    }

    private static IEnumerable<ItemType> GetItemTypes()
    {
      return Engine.Assets.GetAssetsOfType<ItemType>().Cast<ItemType>();
    }
  }
}
