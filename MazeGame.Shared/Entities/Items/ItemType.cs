﻿using MazeGame.Assets.DataLoading;
using MazeGame.Graphics;
using Newtonsoft.Json;

namespace MazeGame.Entities.Items
{
  public class ItemType : INamedJsonItem
  {
    [JsonConverter(typeof(SpriteConverter))]
    public Sprite Sprite { get; }

    public string Name { get; }
    public string Value { get; }
    public int SpawnWeight { get; }
    public string Action { get; }
    public int SpawnLevel { get; }

    public ItemType(string name, Sprite sprite, string value, int spawnWeight, string action, int spawnLevel)
    {
      Name = name;
      Sprite = sprite;
      Value = value;
      SpawnWeight = spawnWeight;
      Action = action;
      SpawnLevel = spawnLevel;
    }
  }
}
