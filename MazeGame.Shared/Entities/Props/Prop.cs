﻿using MazeGame.Graphics;

namespace MazeGame.Entities.Props
{
  public class Prop : Entity
  {
    public bool IsSolid { get; }

    public Prop(string name, bool isSolid, Sprite sprite) : base(name, sprite)
    {
      IsSolid = isSolid;
    }
  }
}
