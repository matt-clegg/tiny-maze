﻿using MazeGame.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MazeGame.Entities.Props
{
  public class Door : Prop
  {
    public bool IsOpen { get; set; }

    private readonly bool _isVertical;

    private readonly Sprite _openSpriteHor;
    private readonly Sprite _closedSpriteHor;

    private readonly Sprite _openSpriteVerTop;
    private readonly Sprite _openSpriteVerBottom;
    private readonly Sprite _closedSpriteVerTop;
    private readonly Sprite _closedSpriteVerBottom;

    public Door(string name, bool isVertical) : base(name, true, null)
    {
      _isVertical = isVertical;

      _openSpriteHor = Engine.Assets.GetAsset<Sprite>("door_open_h");
      _closedSpriteHor = Engine.Assets.GetAsset<Sprite>("door_closed_h");

      // Vertical doors are made up of multiple sprites
      _openSpriteVerTop = Engine.Assets.GetAsset<Sprite>("door_open_top_v");
      _openSpriteVerBottom = Engine.Assets.GetAsset<Sprite>("door_open_bottom_v");

      _closedSpriteVerTop = Engine.Assets.GetAsset<Sprite>("door_closed_top_v");
      _closedSpriteVerBottom = Engine.Assets.GetAsset<Sprite>("door_closed_bottom_v");
    }

    public override void Draw(SpriteBatch batch)
    {
      if (_isVertical)
      {
        DrawVertical(batch);
      }
      else
      {
        DrawHorizontal(batch);
      }
    }

    private void DrawVertical(SpriteBatch batch)
    {
      DrawSprite(IsOpen ? _openSpriteVerTop : _closedSpriteVerTop, X, Y - 1, batch, offsetY: 1);
      DrawSprite(IsOpen ? _openSpriteVerBottom : _closedSpriteVerBottom, X, Y, batch);
    }

    private void DrawHorizontal(SpriteBatch batch)
    {
      Sprite sprite = IsOpen ? _openSpriteHor : _closedSpriteHor;
      DrawSprite(sprite, X, Y, batch);
    }

    private void DrawSprite(Sprite sprite, int x, int y, SpriteBatch batch, int offsetX = 0, int offsetY = 0)
    {
      var destination = new Rectangle(x * sprite.Width + offsetX, y * sprite.Height + offsetY, sprite.Width, sprite.Height);
      batch.Draw(sprite.Texture, destination, sprite.Bounds, Color.White);
    }
  }
}
