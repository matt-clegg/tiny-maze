﻿using MazeGame.Action;
using MazeGame.Entities.Items;
using MazeGame.Input;
using MazeGame.Util;
using MazeGame.World;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace MazeGame.Entities.Creatures
{
  public class Player : Creature
  {
    private IAction _nextAction;

    private int _wealth;
    public int Wealth
    {
      get => _wealth;
      set
      {
        _wealth = value;
        if (_wealth < 0)
        {
          _wealth = 0;
        }
      }
    }

    public bool HasWon
    {
      get;
      private set;
    }

    public List<Item> ActionItems { get; } = new List<Item>();
    public int SelectedItemIndex { get; set; }
    public Item SelectedItem => SelectedItemIndex >= 0 && SelectedItemIndex < ActionItems.Count ? ActionItems[SelectedItemIndex] : null;

    public event EventHandler RestartTriggerEvent;

    public Player(int speed, string name) : base(name, speed, 0, null)
    {
      TurnFinishedEvent += OnTurnFinished;
      RestartTriggerEvent += OnRestart;
      CanChangeRooms = true;
    }

    /// <summary>
    /// Called when the player exits the maze and restarts the game.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void OnRestart(object sender, EventArgs e)
    {
      Wealth = 0;
      CurrentLevel = 0;
      SelectedItemIndex = 0;
      ActionItems.Clear();
      _energy.Reset();
      Timer.Reset();
      HasWon = false;
      _nextAction = null;
    }

    public override bool IsWaitingForInput() => _nextAction == null;

    protected override IAction OnGetAction()
    {
      IAction action = _nextAction;
      _nextAction = null;
      return action;
    }

    public void LearnAction(Item item)
    {
      ActionItems.Add(item);
    }

    public void SelectItem(int index)
    {
      SelectedItemIndex = index;
      if (SelectedItemIndex < 0)
      {
        SelectedItemIndex = 0;
      }

      if (SelectedItemIndex >= ActionItems.Count - 1)
      {
        SelectedItemIndex = ActionItems.Count - 1;
      }
    }

    public void ChangeLevel(int newLevel)
    {
      if (newLevel == Maze.TotalLevels)
      {
        HasWon = true;
        return;
      }

      Room newRoom = Maze.GetLevel(newLevel).StartingRoom;
      ChangeRoom(newRoom);
    }

    private void OnTurnFinished(object sender, EventArgs e)
    {
      if (CurrentRoom.IsEmpty())
      {
        CurrentRoom.OpenPassages();
      }

      if (HasWon)
      {
        Timer.End();
      }
    }

    public void Input(Keys key)
    {
      if (HasWon)
      {
        if (Controls.Restart.IsPressed(key)) RestartTriggerEvent?.Invoke(this, EventArgs.Empty);
        return;
      }

      if (Controls.North.IsPressed(key)) SetNextAction(new MoveAction(0, -1));
      else if (Controls.South.IsPressed(key)) SetNextAction(new MoveAction(0, 1));
      else if (Controls.East.IsPressed(key)) SetNextAction(new MoveAction(1, 0));
      else if (Controls.West.IsPressed(key)) SetNextAction(new MoveAction(-1, 0));

      else if (Controls.UseStairs.IsPressed(key)) SetNextAction(new UseStairs(X, Y));

      else if (Controls.SelectItemDown.IsPressed(key)) SetNextAction(new ChangeItemAction(-1));
      else if (Controls.SelectItemUp.IsPressed(key)) SetNextAction(new ChangeItemAction(1));

      else if (Controls.One.IsPressed(key)) SetNextAction(new ChangeItemAction(newIndex: 0));
      else if (Controls.Two.IsPressed(key)) SetNextAction(new ChangeItemAction(newIndex: 1));
      else if (Controls.Three.IsPressed(key)) SetNextAction(new ChangeItemAction(newIndex: 2));
      else if (Controls.Four.IsPressed(key)) SetNextAction(new ChangeItemAction(newIndex: 3));
      else if (Controls.Five.IsPressed(key)) SetNextAction(new ChangeItemAction(newIndex: 4));
      else if (Controls.Six.IsPressed(key)) SetNextAction(new ChangeItemAction(newIndex: 5));
      else if (Controls.Seven.IsPressed(key)) SetNextAction(new ChangeItemAction(newIndex: 6));
      else if (Controls.Eight.IsPressed(key)) SetNextAction(new ChangeItemAction(newIndex: 7));
      else if (Controls.Nine.IsPressed(key)) SetNextAction(new ChangeItemAction(newIndex: 8));
    }

    private void SetNextAction(IAction action) => _nextAction = action;
  }
}
