﻿using MazeGame.Util;
using System.Collections.Generic;
using System.Linq;

namespace MazeGame.Entities.Creatures
{
  public static class CreatureGenerator
  {

    /// <summary>
    /// Create a new creature instance for a given level.
    /// </summary>
    /// <param name="level"></param>
    /// <param name="random"></param>
    /// <returns></returns>
    public static Creature GetThreatForLevel(int level, IRandom random)
    {
      IEnumerable<CreatureType> types = GetCreatureTypes().Where(type => type.SpawnLevel - 1 <= level);
      CreatureType creatureType = types.OrderBy(x => random.Next()).FirstOrDefault();
      return creatureType != null ? Creature.FromType(creatureType) : null;
    }

    private static IEnumerable<CreatureType> GetCreatureTypes()
    {
      return Engine.Assets.GetAssetsOfType<CreatureType>().Cast<CreatureType>();
    }
  }
}
