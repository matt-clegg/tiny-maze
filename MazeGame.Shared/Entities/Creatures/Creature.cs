﻿using MazeGame.Action;
using MazeGame.Ai;
using MazeGame.Entities.Items;
using MazeGame.Entities.Props;
using MazeGame.Graphics;
using MazeGame.Turns;
using MazeGame.Util;
using MazeGame.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace MazeGame.Entities.Creatures
{
  public class Creature : Entity, ITurnable
  {
    private const int RenderOffsetY = -7;

    protected readonly Energy _energy = new Energy();
    private readonly AnimationSet<Point2D> _animationSet;

    public int Speed { get; }
    public int WealthDamage { get; }
    public string DefeatAction { get; }

    public CreatureAi Ai { private get; set; }
    public bool CanChangeRooms { get; protected set; }

    public event EventHandler TurnFinishedEvent;
    public event EventHandler RoomChangedEvent;

    public Creature(string name, int speed, int wealthDamage, string defeatAction) : base(name, null)
    {
      _animationSet = CreatureType.CreateAnimationGroup(name, 500);
      Speed = speed;
      WealthDamage = wealthDamage;
      DefeatAction = defeatAction;
      CanChangeRooms = false;
    }

    public override Sprite Sprite => _animationSet.CurrentAnimation.GetSprite;

    public bool CanTakeTurn() => !ShouldRemove && _energy.CanTakeTurn;
    public bool GainEnergy() => _energy.Gain(Speed);
    public virtual bool IsWaitingForInput() => false;

    public IAction GetAction() => OnGetAction();
    protected virtual IAction OnGetAction() => Ai.DecideNextAction();

    public void Update(GameTime gameTime)
    {
      _animationSet.Update(gameTime);
    }

    public void FinishTurn()
    {
      if (ShouldRemove)
      {
        return;
      }

      _energy.Spend();
      TurnFinishedEvent?.Invoke(this, EventArgs.Empty);
    }

    /// <summary>
    /// Try and move the creature by a specified amount on each axis.
    /// </summary>
    /// <param name="dx"></param>
    /// <param name="dy"></param>
    /// <returns>Whether or not the creature moved.</returns>
    public bool TryMoveBy(int dx, int dy)
    {
      if (dx == 0 && dy == 0)
      {
        return false;
      }

      int newX = X + dx;
      int newY = Y + dy;

      Tile tile = CurrentRoom.GetTile(newX, newY);
      Prop prop = CurrentRoom.GetProp(newX, newY);

      bool hasMoved = Ai.OnEnter(newX, newY, tile, prop);

      if (hasMoved)
      {
        // Set the current animation based on the direction of movement
        _animationSet.SetKey(new Point2D(dx, dy));
      }

      return hasMoved;
    }

    /// <summary>
    /// Attack this creature with the specified item.
    /// </summary>
    /// <param name="item"></param>
    /// <returns>Whether or not the attack was successful.</returns>
    public bool Attack(Item item)
    {
      if (item.Action.Equals(DefeatAction))
      {
        ShouldRemove = true;
        CurrentRoom.AddBlood(X, Y);
        return true;
      }
      else
      {
        return false;
      }
    }

    /// <summary>
    /// Try and enter the room that connects to a specified passageway in this room.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns>Whether or not the creature changed room.</returns>
    public bool TryEnterRoomAt(int x, int y)
    {
      Room newRoom = CurrentRoom.GetPassageConnectionAt(x, y);
      if (newRoom != null)
      {
        ChangeRoom(newRoom);
        return true;
      }

      return false;
    }

    protected void ChangeRoom(Room newRoom)
    {
      RoomChangedEvent?.Invoke(this, new RoomChangeEventArgs(CurrentRoom, newRoom));
      CurrentRoom.RemoveCreature(this);

      Point2D roomSpawn = newRoom.GetSpawnFrom(CurrentRoom);

      newRoom.AddFootprintAtTile(roomSpawn.X, roomSpawn.Y);
      newRoom.AddCreature(this, roomSpawn);

      CurrentLevel = newRoom.Level;
    }

    public override void Draw(SpriteBatch batch)
    {
      batch.Draw(Sprite.Texture, new Rectangle(X * Sprite.Width, Y * Sprite.Height + RenderOffsetY, Sprite.Width, Sprite.Height), Sprite.Bounds, Color.White);
    }

    /// <summary>
    /// Creates a new creature instance from a specified <see cref="CreatureType"/>.
    /// </summary>
    /// <param name="type"></param>
    /// <returns>The new Creature instance.</returns>
    public static Creature FromType(CreatureType type)
    {
      return new Creature(type.Name, type.Speed, type.WealthDamage, type.DefeatAction);
    }
  }
}
