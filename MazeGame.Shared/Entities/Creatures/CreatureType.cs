﻿using MazeGame.Assets.DataLoading;
using MazeGame.Graphics;
using MazeGame.Util;

namespace MazeGame.Entities.Creatures
{
  public class CreatureType : INamedJsonItem
  {
    public string Name { get; }
    public int Speed { get; }
    public int WealthDamage { get; }
    public string DefeatAction { get; }
    public int SpawnLevel { get; }

    public CreatureType(string name, int speed, int wealthDamage, string defeatAction, int spawnLevel)
    {
      Name = name;
      Speed = speed;
      WealthDamage = wealthDamage;
      DefeatAction = defeatAction;
      SpawnLevel = spawnLevel;
    }

    public static AnimationSet<Point2D> CreateAnimationGroup(string name, int frameDurationMs, int startingFrame = 0)
    {
      var group = new AnimationSet<Point2D>();

      group.AddAnimation(Direction.South,
                     new AnimatedSprite(frameDurationMs, startingFrame,
                                        Engine.Assets.GetAsset<Sprite>($"{name}_bottom_1"),
                                        Engine.Assets.GetAsset<Sprite>($"{name}_bottom_2")));
      group.AddAnimation(Direction.West,
                     new AnimatedSprite(frameDurationMs, startingFrame,
                                        Engine.Assets.GetAsset<Sprite>($"{name}_left_1"),
                                        Engine.Assets.GetAsset<Sprite>($"{name}_left_2")));
      group.AddAnimation(Direction.North,
                     new AnimatedSprite(frameDurationMs, startingFrame,
                                        Engine.Assets.GetAsset<Sprite>($"{name}_top_1"),
                                        Engine.Assets.GetAsset<Sprite>($"{name}_top_2")));
      group.AddAnimation(Direction.East,
                     new AnimatedSprite(frameDurationMs, startingFrame,
                                        Engine.Assets.GetAsset<Sprite>($"{name}_right_1"),
                                        Engine.Assets.GetAsset<Sprite>($"{name}_right_2")));
      return group;
    }

  }
}
