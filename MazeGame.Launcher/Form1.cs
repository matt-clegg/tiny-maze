﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace MazeGame.Launcher
{
    public partial class Form1 : Form
    {
        private const string DirectX = "directx";
        private const string OpenGL = "opengl";

        private const string DirectXFile = @"DirectX\Tiny Maze DirectX.exe";
        private const string OpenGLFile = @"OpenGL\Tiny Maze OpenGL.exe";

        private string VersionType {
            get => Properties.Settings.Default.Type.Trim().ToLower();
            set {
                Properties.Settings.Default.Type = value;
                Properties.Settings.Default.Save();
            }
        }

        public Form1()
        {
            InitializeComponent();

            switch (VersionType)
            {
                case DirectX:
                    radioDirectX.Checked = true;
                    break;
                case OpenGL:
                    radioOpenGL.Checked = true;
                    break;
                default:
                    ShowError($"Invalid version type: {VersionType}. Must be '{DirectX}' or '{OpenGL}'. Defaulting to {DirectX}.", "Settings Error");

                    radioDirectX.Checked = true;
                    break;
            }
        }

        private void btnLaunch_Click(object sender, EventArgs e)
        {
            if (radioDirectX.Checked)
            {
                VersionType = DirectX;
                StartProcess(DirectXFile);
            }
            else if (radioOpenGL.Checked)
            {
                VersionType = OpenGL;
                StartProcess(OpenGLFile);
            }
            else
            {
                ShowError("Please select a version.", null, MessageBoxIcon.Exclamation);
            }
        }

        private void StartProcess(string processName)
        {
            string currentPath = Path.GetDirectoryName(Application.ExecutablePath);

            if (string.IsNullOrWhiteSpace(currentPath))
            {
                ShowError("Could not get executing assembly path.", null);
                return;
            }

            string processPath = Path.Combine(currentPath, processName);

            if (!File.Exists(processPath))
            {
                ShowError($"Cannot find '{processName}'. File may have moved or been renamed.", "Error");
                return;
            }

            try
            {
                Process process = Process.Start(processPath);

                if (process != null)
                {
                    process.WaitForInputIdle();
                    Application.Exit();
                }
                else
                {
                    ShowError($"Unable to start '{processName}' process.", null);
                }
            }
            catch (Exception ex)
            {
                ShowError($"An error occurred: {ex.Message}.", null);
            }
        }

        private void ShowError(string message, string title, MessageBoxIcon icon = MessageBoxIcon.Error)
        {
            MessageBox.Show(message, title, MessageBoxButtons.OK, icon);
        }
    }
}
