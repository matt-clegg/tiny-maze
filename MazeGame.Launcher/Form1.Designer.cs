﻿namespace MazeGame.Launcher
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioDirectX = new System.Windows.Forms.RadioButton();
            this.radioOpenGL = new System.Windows.Forms.RadioButton();
            this.btnLaunch = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioOpenGL);
            this.groupBox1.Controls.Add(this.radioDirectX);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(227, 84);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Version";
            // 
            // radioDirectX
            // 
            this.radioDirectX.AutoSize = true;
            this.radioDirectX.Checked = true;
            this.radioDirectX.Location = new System.Drawing.Point(16, 27);
            this.radioDirectX.Name = "radioDirectX";
            this.radioDirectX.Size = new System.Drawing.Size(60, 17);
            this.radioDirectX.TabIndex = 0;
            this.radioDirectX.TabStop = true;
            this.radioDirectX.Text = "DirectX";
            this.radioDirectX.UseVisualStyleBackColor = true;
            // 
            // radioOpenGL
            // 
            this.radioOpenGL.AutoSize = true;
            this.radioOpenGL.Location = new System.Drawing.Point(16, 51);
            this.radioOpenGL.Name = "radioOpenGL";
            this.radioOpenGL.Size = new System.Drawing.Size(65, 17);
            this.radioOpenGL.TabIndex = 1;
            this.radioOpenGL.Text = "OpenGL";
            this.radioOpenGL.UseVisualStyleBackColor = true;
            // 
            // btnLaunch
            // 
            this.btnLaunch.Location = new System.Drawing.Point(252, 121);
            this.btnLaunch.Name = "btnLaunch";
            this.btnLaunch.Size = new System.Drawing.Size(75, 23);
            this.btnLaunch.TabIndex = 1;
            this.btnLaunch.Text = "Launch";
            this.btnLaunch.UseVisualStyleBackColor = true;
            this.btnLaunch.Click += new System.EventHandler(this.btnLaunch_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 156);
            this.Controls.Add(this.btnLaunch);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tiny Launcher - v1.0";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioOpenGL;
        private System.Windows.Forms.RadioButton radioDirectX;
        private System.Windows.Forms.Button btnLaunch;
    }
}

