# Tiny Maze

This game was made in 37 hours as a part of my final Software Development apprenticeship.

[Play the game on itch.io!](https://emmsii.itch.io/tiny-maze)

### Project Structure

| Project | Description |
| ------- | ----------- |
| MazeGame.DirectX | Will run the game in DirectX mode. |
| MazeGame.Launcher | A little winforms launcher that allows the user to run either in DirectX or OpenGl mode. |
| MazeGame.OpenGL | Will run the game in OpenGL mode. |
| MazeGame.Shared | The main game code. References by the DirectX and OpenGL projects. |

### Requirements

This game has only been tested on Windows 10. If you run into any issues, [submit a new issue](https://gitlab.com/matt-clegg/tiny-maze/issues). 

The .NET Framework 4.5 is required to build and run.

### Licence

- This project is licenced under the [MIT Licence](https://gitlab.com/matt-clegg/tiny-maze/blob/master/LICENSE).  
- Find the Oryx Design Lab licence [here](https://www.oryxdesignlab.com/license).