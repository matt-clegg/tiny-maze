﻿using System;

namespace MazeGame.OpenGL
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            const string title = "Tiny Maze";
            const int windowWidth = 672;
            const int windowHeight = 416;
            const int virtualWidth = windowWidth / 2;
            const int virtualHeight = windowHeight / 2;

            using (var engine = new Engine(title, windowWidth, windowHeight, virtualWidth, virtualHeight))
            {
                engine.Run();
            }
        }
    }
}
